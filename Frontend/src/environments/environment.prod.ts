export const environment = {
    production: true,
    backendUrl: 'http://localhost:3000',
    cloudinarySettings: {
        cloudName: 'donhvedgr',
        uploadPreset: 'mxjmknsw'
    }
};
