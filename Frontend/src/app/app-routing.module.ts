// Builtin
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Guards
import { LoggedInGuard } from './core/guards/logged-in.guard';
import { LoggedOutGuard } from './core/guards/logged-out.guard';

//Resolver
import { UserResolver } from './core/resolvers/user.resolver';
import { NotFoundPageComponent } from './shared/pages/not-found-page/not-found-page.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'public',
		pathMatch: 'full',
	},
	{
		path: 'public',
		loadChildren: () =>
			import('./public/public.module').then((m) => m.PublicModule),
		canLoad: [LoggedOutGuard],
	},
	{
		path: 'main',
		loadChildren: () =>
			import('./main/main.module').then((m) => m.MainModule),
		canLoad: [LoggedInGuard],
		resolve: {
			user: UserResolver,
		},
	},
	{
		path: '**',
		component: NotFoundPageComponent,
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
