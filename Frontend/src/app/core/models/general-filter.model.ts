export interface GeneralFilterInterface {
	[key: string]: string | number;
}
