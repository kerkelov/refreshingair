export type JwtData = {
	sub: string;
	nameid: string;
	iat: string;
	exp: string;
};
