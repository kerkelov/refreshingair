export interface UserResponse {
	id: string;
	firstName: string;
	lastName: string;
	email: string;
	userName: string;
	phoneNumber: string;
	profilePictureUrl: string;
	role: string;
	location?: string;
	createdOn: string;
}
