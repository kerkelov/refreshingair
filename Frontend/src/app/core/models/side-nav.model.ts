export interface sideNavItem {
	title: string;
	icon: string;
	path: string;
}
