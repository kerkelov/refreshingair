export interface UserRequest {
	firstName: string;
	lastName: string;
	email: string;
	userName: string;
	phoneNumber: string;
	profilePictureUrl: string;
	password: string;
	role: string;
}
