//Builtin
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
//Services
import { LocalStorageUtilService } from './local-storage-util.service';
//Models
import { LoginRequest } from '../models/login-request.model';
import { LoginResponse } from '../models/login-response.model';
//Other
import { environment } from '../../../environments/environment';
import { jwtTokenKey } from '../constants/local-storage-keys';
import { UserRequest } from '../models/user-request.model';
import { UserResponse } from '../models/user-response.model';

@Injectable({
	providedIn: 'root',
})
export class AuthService {
	loginUrl: string = `${environment.backendUrl}/Authentication/login`;
	registerUrl: string = `${environment.backendUrl}/Users/client`;

	constructor(
		private httpClient: HttpClient,
		private localStorageUtil: LocalStorageUtilService
	) {}

	login(credentials: LoginRequest): Observable<LoginResponse> {
		return this.httpClient
			.post<LoginResponse>(this.loginUrl, credentials)
			.pipe(
				tap((response) =>
					this.localStorageUtil.setItem(
						jwtTokenKey,
						response.jwtToken
					)
				)
			);
	}

	logout(): void {}

	register(user: UserRequest): Observable<UserResponse> {
		return this.httpClient.post<UserResponse>(this.registerUrl, user);
	}
}
