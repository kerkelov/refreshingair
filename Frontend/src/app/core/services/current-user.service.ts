import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UsersService } from '../../main/users/services/users.service';
import { Role } from '../enums/role';
import { UserResponse } from '../models/user-response.model';

@Injectable({
	providedIn: 'root',
})
export class CurrentUserService {
	private currentUserSubject: BehaviorSubject<UserResponse | null> =
		new BehaviorSubject<UserResponse | null>(null);

	currentUserObservable: Observable<UserResponse | null> =
		this.currentUserSubject.asObservable();

	constructor(usersService: UsersService) {}

	get currentUser(): UserResponse | null {
		return this.currentUserSubject.getValue();
	}

	get isClient(): boolean {
		return this.hasRole(Role.Client);
	}

	get isTechnician(): boolean {
		return this.hasRole(Role.Technician);
	}

	get isAdmin(): boolean {
		return this.hasRole(Role.Admin);
	}

	setCurrentUser(user: UserResponse): void {
		if (user === null) {
			this.clearCurrentUser();
			return;
		}

		this.currentUserSubject.next(user);
	}

	clearCurrentUser(): void {
		this.currentUserSubject.next(null);
	}

	private hasRole(role: Role): boolean {
		const currentUserRole: Role | undefined =
			this.currentUserSubject.getValue()?.role as Role;

		if (currentUserRole === undefined) {
			return false;
		}

		return currentUserRole === role;
	}
}
