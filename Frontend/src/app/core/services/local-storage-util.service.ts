import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class LocalStorageUtilService {
	getItem(key: string): number | string | Record<string, any> | null {
		const theValueInTheLocalStorage = localStorage.getItem(key);

		if (theValueInTheLocalStorage === null) {
			return null;
		}

		//JSON.parse throws error when passed normal string like 'gosho' or 'javascript'
		try {
			return JSON.parse(theValueInTheLocalStorage);
		} catch {
			return theValueInTheLocalStorage;
		}
	}

	setItem(key: string, item: Record<string, any> | number | string): void {
		if (typeof item === 'string') {
			localStorage.setItem(key, item);
		} else {
			localStorage.setItem(key, JSON.stringify(item));
		}
	}

	deleteItem(key: string): void {
		localStorage.removeItem(key);
	}

	clearStorage(): void {
		localStorage.clear();
	}
}
