import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { CurrentUserService } from '../services/current-user.service';

@Injectable({
	providedIn: 'root',
})
export class AdminGuard implements CanActivate {
	constructor(
		private currentUserService: CurrentUserService,
		private router: Router
	) {}

	canActivate(): UrlTree | boolean {
		if (!this.currentUserService.isAdmin) {
			// return false;
		}

		return true;
	}
}
