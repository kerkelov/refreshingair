import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { jwtTokenKey } from '../constants/local-storage-keys';
import { LocalStorageUtilService } from '../services/local-storage-util.service';

@Injectable({
	providedIn: 'root',
})
export class LoggedOutGuard implements CanLoad {
	constructor(
		private readonly router: Router,
		private localStorageUtilService: LocalStorageUtilService
	) {}

	canLoad(
		route: Route,
		segments: UrlSegment[]
	):
		| Observable<boolean | UrlTree>
		| Promise<boolean | UrlTree>
		| boolean
		| UrlTree {
		if (this.localStorageUtilService.getItem(jwtTokenKey)) {
			this.router.navigate(['main/users']);
			return false;
		} else {
			return true;
		}
	}
}
