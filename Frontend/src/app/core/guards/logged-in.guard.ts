import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { jwtTokenKey } from '../constants/local-storage-keys';
import { LocalStorageUtilService } from '../services/local-storage-util.service';

@Injectable({
	providedIn: 'root',
})
export class LoggedInGuard implements CanLoad {
	constructor(
		private readonly router: Router,
		private localStorage: LocalStorageUtilService
	) {}

	canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree {
		if (this.localStorage.getItem(jwtTokenKey)) {
			return true;
		} else {
			this.router.navigate(['public/access-denied']);
			return false;
		}
	}
}
