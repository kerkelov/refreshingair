//Must be at least 6 characters long with a number and special symbol
export const passwordRegex =
	'^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-+_(){}<>,;]).{6,}$';
