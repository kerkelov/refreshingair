import { sideNavItem } from '../models/side-nav.model';

export const sideNavItems: sideNavItem[] = [
	{ title: 'Users', icon: 'account_circle', path: '/main/users' },
	{
		title: 'Air conditioners',
		icon: 'air',
		path: '/main/air-conditioners',
	},
	{ title: 'Orders', icon: 'bookmark_border', path: '/main/orders' },
	{ title: 'Repairs', icon: 'construction', path: '/main/repairs' },
];
