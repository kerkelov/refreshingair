import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import jwt_decode from 'jwt-decode';
import { Observable, take, tap } from 'rxjs';
import { UsersService } from '../../main/users/services/users.service';
import { jwtTokenKey } from '../constants/local-storage-keys';
import { JwtData } from '../models/jwt-data.model';
import { UserResponse } from '../models/user-response.model';
import { CurrentUserService } from '../services/current-user.service';
import { LocalStorageUtilService } from '../services/local-storage-util.service';

@Injectable({
	providedIn: 'root',
})
export class UserResolver implements Resolve<UserResponse> {
	constructor(
		private usersService: UsersService,
		private currentUserService: CurrentUserService,
		private localStorageUtil: LocalStorageUtilService
	) {}
	resolve(): Observable<UserResponse> {
		const jwtFromLocalStorage = this.localStorageUtil.getItem(
			jwtTokenKey
		) as string;

		const jwtData: JwtData = jwt_decode<JwtData>(jwtFromLocalStorage);

		return this.usersService.getUserById(jwtData.nameid).pipe(
			take(1),
			tap((user) => this.currentUserService.setCurrentUser(user))
		);
	}
}
