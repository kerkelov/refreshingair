export enum AirConditionerType {
	Portable = 'Portable',
	Window = 'Window',
	Central = 'Central',
}
