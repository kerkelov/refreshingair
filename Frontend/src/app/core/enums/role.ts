export enum Role {
	Client = 'Client',
	Technician = 'Technician',
	Admin = 'Administrator',
}
