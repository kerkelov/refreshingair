import {
	HttpErrorResponse,
	HttpEvent,
	HttpHandler,
	HttpInterceptor,
	HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, tap } from 'rxjs';
import { jwtTokenKey } from '../constants/local-storage-keys';
import { LocalStorageUtilService } from '../services/local-storage-util.service';

@Injectable({
	providedIn: 'root',
})
export class JwtTokenInterceptor implements HttpInterceptor {
	constructor(
		private localStorageUtil: LocalStorageUtilService,
		private router: Router
	) {}

	intercept(
		req: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
		const jwt = this.localStorageUtil.getItem(jwtTokenKey);

		if (jwt) {
			req = req.clone({
				setHeaders: {
					Authorization: `Bearer ${jwt}`,
				},
			});
		}

		return next.handle(req).pipe(
			tap({
				next: () => {},
				error: (err: any) => {
					if (
						err instanceof HttpErrorResponse &&
						err.status === 401
					) {
						this.localStorageUtil.clearStorage();
						this.router.navigate(['/welcome']);
					}
				},
			})
		);
	}
}
