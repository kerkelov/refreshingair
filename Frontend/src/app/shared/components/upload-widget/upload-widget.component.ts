import { Component, EventEmitter, Input, Output } from '@angular/core';
import { environment } from '../../../../environments/environment';

const cloudName = environment.cloudinarySettings.cloudName,
    uploadPreset = environment.cloudinarySettings.uploadPreset;

@Component({
    selector: 'app-upload-widget',
    templateUrl: './upload-widget.component.html',
    styleUrls: ['./upload-widget.component.scss']
})
export class UploadWidgetComponent {
    @Input() text: string = 'Upload';
    @Output() newPictureUploaded = new EventEmitter<string>();

    //@ts-ignore
    widget = cloudinary.createUploadWidget(
        {
            cloudName,
            uploadPreset
        },
        (error: Error, uploadResult: any) => {
            if (!error && uploadResult && uploadResult.event === 'success') {
                const pictureUrl =
                    `https://res.cloudinary.com/${cloudName}/image/upload/` +
                    uploadResult.info.path;

                this.newPictureUploaded.emit(pictureUrl);
            }
        }
    );

    showWidget(): void {
        this.widget.open();
    }
}
