export type Message = {
    _id: string;
    message: string;
    senderName: string;
    senderEmail: string;
};
