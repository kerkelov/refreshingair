import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import {
	MAT_SNACK_BAR_DEFAULT_OPTIONS,
	MatSnackBarModule,
} from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { AboutSectionComponent } from './components/about-section/about-section.component';
import { BlogComponent } from './components/blog/blog.component';
import { BlogsSectionComponent } from './components/blogs-section/blogs-section.component';
import { ContactUsFormComponent } from './components/contact-us-form/contact-us-form.component';
import { ContactUsSectionComponent } from './components/contact-us-section/contact-us-section.component';
import { FeatureComponent } from './components/feature/feature.component';
import { FeaturesSectionComponent } from './components/features-section/features-section.component';
import { FormsSocialsComponent } from './components/forms-socials/forms-socials.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { MainSectionComponent } from './components/main-section/main-section.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { ReviewComponent } from './components/review/review.component';
import { ReviewsSectionComponent } from './components/reviews-section/reviews-section.component';
import { LandingPageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { PublicLayoutModule } from './public-layout/public-layout.module';
import { PublicRoutingModule } from './public-routing.module';

@NgModule({
	declarations: [
		RegisterPageComponent,
		LandingPageComponent,
		RegisterFormComponent,
		RegisterFormComponent,
		LoginPageComponent,
		LoginFormComponent,
		MainSectionComponent,
		AboutSectionComponent,
		FeaturesSectionComponent,
		ReviewsSectionComponent,
		ContactUsSectionComponent,
		BlogsSectionComponent,
		FeatureComponent,
		ReviewComponent,
		BlogComponent,
		ContactUsFormComponent,
		FormsSocialsComponent,
	],
	imports: [
		RouterModule,
		CommonModule,
		ReactiveFormsModule,
		MatNativeDateModule,
		MatButtonModule,
		MatInputModule,
		MatFormFieldModule,
		MatRadioModule,
		MatDatepickerModule,
		MatSnackBarModule,
		PublicRoutingModule,
		MatIconModule,
		SharedModule,
		PublicLayoutModule,
	],
	providers: [
		{
			provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
			useValue: { duration: 3000, panelClass: 'snack-bar' },
		},
	],
})
export class PublicModule {}
