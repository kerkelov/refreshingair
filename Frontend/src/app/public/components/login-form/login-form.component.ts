import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { take } from 'rxjs';
import { jwtTokenKey } from '../../../core/constants/local-storage-keys';
import { passwordRegex } from '../../../core/constants/regexes';
import { UserResponse } from '../../../core/models/user-response.model';
import { AuthService } from '../../../core/services/auth.service';
import { CurrentUserService } from '../../../core/services/current-user.service';
import { getErrorMessage } from '../../../core/utils/get-error-message';
import { UsersService } from '../../../main/users/services/users.service';

@Component({
	selector: 'app-login-form',
	templateUrl: './login-form.component.html',
	styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
	loginForm!: FormGroup;
	isPasswordVisible: boolean = false;

	constructor(
		private formBuilder: FormBuilder,
		private authService: AuthService,
		private snackBar: MatSnackBar,
		private router: Router,
		private currentUserService: CurrentUserService,
		private usersService: UsersService
	) {}

	get emailControl(): AbstractControl | null {
		return this.loginForm.get('email');
	}

	get passwordControl(): AbstractControl | null {
		return this.loginForm.get('password');
	}

	ngOnInit(): void {
		this.loginForm = this.formBuilder.group({
			email: new FormControl('', [Validators.required, Validators.email]),
			password: new FormControl('', [
				Validators.required,
				Validators.pattern(passwordRegex),
			]),
		});
	}

	onSubmit(): void {
		const observer = {
			next: () => {
				let jwt = localStorage.getItem(jwtTokenKey);
				let decoded = jwtDecode(jwt!);
				let userId = (decoded as { nameid: string })['nameid'];

				let getByIdObserver = {
					next: (user: UserResponse) => {
						this.currentUserService.setCurrentUser(user);
						if (this.currentUserService.isAdmin) {
							this.router.navigate(['main']);
						} else {
							this.router.navigate(['main/orders']);
						}
					},
				};

				this.usersService
					.getUserById(userId)
					.subscribe(getByIdObserver);
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(getErrorMessage(httpError), 'Close');
			},
		};

		this.authService
			.login(this.loginForm.value)
			.pipe(take(1))
			.subscribe(observer);
	}

	togglePasswordVisibility(): void {
		this.isPasswordVisible = !this.isPasswordVisible;
	}
}
