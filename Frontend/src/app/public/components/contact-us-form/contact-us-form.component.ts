import { Component, OnInit } from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs';
import { Message } from '../../../shared/models/message.model';
import { MessagesService } from '../../services/messages.service';

@Component({
	selector: 'app-contact-us-form',
	templateUrl: './contact-us-form.component.html',
	styleUrls: ['./contact-us-form.component.scss'],
})
export class ContactUsFormComponent implements OnInit {
	contactUsForm!: FormGroup;

	constructor(
		private messagesService: MessagesService,
		private formBuilder: FormBuilder,
		private snackBar: MatSnackBar
	) {}

	get messageControl(): AbstractControl | null {
		return this.contactUsForm.get('message');
	}

	get emailControl(): AbstractControl | null {
		return this.contactUsForm.get('senderEmail');
	}

	get nameControl(): AbstractControl | null {
		return this.contactUsForm.get('senderName');
	}

	ngOnInit(): void {
		this.contactUsForm = this.formBuilder.group({
			message: new FormControl('', [Validators.required]),
			senderEmail: new FormControl('', [
				Validators.required,
				Validators.email,
			]),
			senderName: new FormControl('', [Validators.required]),
		});
	}

	onSubmit(): void {
		const observer = {
			next: (message: Message) => {
				this.snackBar.open('Message successfully sent!', 'Close', {
					panelClass: 'round-white-background',
				});
			},
			error: (error: Error) => {
				this.snackBar.open(
					'Error happened, message not sent.',
					'Close',
					{ panelClass: 'round-white-background' }
				);
			},
		};

		this.messagesService
			.sendMessage(this.contactUsForm.value)
			.pipe(take(1))
			.subscribe(observer);
	}
}
