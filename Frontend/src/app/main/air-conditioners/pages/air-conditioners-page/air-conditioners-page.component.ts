import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, finalize, switchMap, takeUntil } from 'rxjs';
import { Role } from '../../../../core/enums/role';
import { CurrentUserService } from '../../../../core/services/current-user.service';
import { AbstractPaginator } from '../../components/abstractions/abstract-paginator';
import { CreateAirConditionerFormComponent } from '../../components/create-air-conditioner-form/create-air-conditioner-form.component';
import { AirConditionerPagableResponse } from '../../models/air-conditioner-pagable-response.model';
import { AirConditionerResponse } from '../../models/air-conditioner-response.model';
import { AirConditionersService } from '../../services/air-conditioners.service';

@Component({
	selector: 'app-air-conditioners',
	templateUrl: './air-conditioners-page.component.html',
	styleUrls: [
		'./air-conditioners-page.component.scss',
		'../../../../../assets/styles/gradient-button.scss',
	],
})
export class AirConditionersPageComponent
	extends AbstractPaginator<AirConditionerPagableResponse>
	implements OnInit, AfterViewInit
{
	airConditioners: AirConditionerResponse[] = [];
	isLoading: boolean = false;
	filterForm!: FormGroup;
	roles: string[] = Object.values(Role);
	isListDisplayed!: boolean;

	constructor(
		private airConditionersService: AirConditionersService,
		private snackBar: MatSnackBar,
		private dialog: MatDialog,
		private formBuilder: FormBuilder,
		private activatedRoute: ActivatedRoute,
		private titleService: Title,
		public currentUserService: CurrentUserService
	) {
		super();
		this.titleService.setTitle('RefreshingAir | Users');
	}

	get modelControl(): AbstractControl | null {
		return this.filterForm.get('model');
	}

	get manufacturerControl(): AbstractControl | null {
		return this.filterForm.get('manufacturer');
	}

	get countryControl(): AbstractControl | null {
		return this.filterForm.get('country');
	}

	get priceControl(): AbstractControl | null {
		return this.filterForm.get('price');
	}

	get pictureUrlControl(): AbstractControl | null {
		return this.filterForm.get('pictureUrl');
	}

	ngOnInit(): void {
		this.activatedRoute.paramMap
			.pipe(
				switchMap((params) => {
					this.createFilterForm(params);
					return this.getData(this.page, this.size);
				}),
				takeUntil(this.destroy$)
			)
			.subscribe(this.getObserver());

		this.airConditionersService.airConditionerUpdatedObservable.subscribe(
			() => {
				return this.getData(this.page, this.size).subscribe(
					this.getObserver()
				);
			}
		);
	}

	ngAfterViewInit(): void {
		this.subscribeToPaginatorChanges()
			.pipe(takeUntil(this.destroy$))
			.subscribe(this.getObserver());
	}

	createFilterForm(params: ParamMap): void {
		this.filterForm = this.formBuilder.group({
			model: [params.get('model') || ''],
			country: [params.get('country') || ''],
			manufacturer: [params.get('manufacturer') || ''],
		});
	}

	applyFilters(reset?: boolean): void {
		this.paginator.pageIndex = 0;
		this.page = 0;

		this.modelControl?.setValue((this.modelControl.value || '').trim());
		this.countryControl?.setValue((this.countryControl.value || '').trim());
		this.manufacturerControl?.setValue(
			(this.manufacturerControl.value || '').trim()
		);

		if (reset) {
			this.filterForm.reset();
		}

		this.getData(this.page, this.size).subscribe(this.getObserver());
	}

	openCreateAirConditionerDialog(): void {
		const dialogRef = this.dialog.open(CreateAirConditionerFormComponent, {
			width: '60vw',
			autoFocus: false,
			disableClose: true,
			panelClass: ['round-without-padding', 'modal-container'],
		});

		dialogRef
			.afterClosed()
			.pipe(
				switchMap(() => this.getData(this.page, this.size)),
				takeUntil(this.destroy$)
			)
			.subscribe(this.getObserver());
	}

	onDeleted(airConditioner: AirConditionerResponse): void {
		this.getData(this.page, this.size).subscribe(this.getObserver());
		this.snackBar.open(
			`Air conditioner ${airConditioner.model} is deleted`,
			'Close'
		);
	}

	getObserver(): {
		next: (res: AirConditionerPagableResponse) => void;
		error: () => void;
	} {
		return {
			next: (res: AirConditionerPagableResponse) => {
				this.airConditioners = res.content;
				this.count = res.content.length;
			},
			error: () => {
				console.log('possibly error occured');
			},
		};
	}

	getData(
		page: number,
		size: number
	): Observable<AirConditionerPagableResponse> {
		this.isLoading = true;

		return this.airConditionersService
			.getAirConditioners(this.filterForm.value, page, size)
			.pipe(
				finalize(() => (this.isLoading = false)),
				takeUntil(this.destroy$)
			);
	}
}
