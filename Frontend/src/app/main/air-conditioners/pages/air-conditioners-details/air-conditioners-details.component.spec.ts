import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AirConditionerDetailsComponent } from './air-conditioners-details.component';

describe('AirConditionerProfileComponent', () => {
	let component: AirConditionerDetailsComponent;
	let fixture: ComponentFixture<AirConditionerDetailsComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AirConditionerDetailsComponent],
		}).compileComponents();

		fixture = TestBed.createComponent(AirConditionerDetailsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
