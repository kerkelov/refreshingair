//Angular
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
//Services
import { AirConditionersService } from '../../services/air-conditioners.service';
//Material
import { MatSnackBar } from '@angular/material/snack-bar';
import { AirConditionerResponse } from '../../models/air-conditioner-response.model';

@Component({
	selector: 'app-air-conditioner-details',
	templateUrl: './air-conditioners-details.component.html',
	styleUrls: ['./air-conditioners-details.component.scss'],
})
export class AirConditionerDetailsComponent implements OnInit, OnDestroy {
	airConditioner: AirConditionerResponse | null = null;
	id!: string;
	destroy$: Subject<void> = new Subject();

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		public airConditionersService: AirConditionersService,
		private snackBar: MatSnackBar,
		private titleService: Title //TODO: add it // private ordersService: OrdersService
	) {
		this.titleService.setTitle('Refreshing Air | Profile');
	}

	ngOnInit(): void {
		this.fetchairConditioner();
		this.airConditionersService.airConditionerUpdatedObservable
			.pipe(takeUntil(this.destroy$))
			.subscribe({
				next: () => this.fetchairConditioner(),
			});
	}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.unsubscribe();
	}

	fetchairConditioner(): void {
		this.id = this.route.snapshot.paramMap.get('id')!;
		this.airConditionersService
			.getById(this.id)
			.pipe(takeUntil(this.destroy$))
			.subscribe(
				(res) => {
					this.airConditioner = res;
				},
				() => {
					this.router.navigate(['main/air-conditioners']);
					this.snackBar.open(`Air conditioner not found!`, 'Close', {
						duration: 4000,
					});
				}
			);
	}

	onDelete(airConditioner: AirConditionerResponse): void {
		this.airConditioner = null;

		this.snackBar.open(
			`Air conditioner ${airConditioner.model} is deleted`,
			'Close'
		);

		this.router.navigate(['/main/air-conditioners']);
	}
}
