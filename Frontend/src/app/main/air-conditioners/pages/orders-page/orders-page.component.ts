import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, forkJoin, map, switchMap, take, tap } from 'rxjs';
import { UserResponse } from '../../../../core/models/user-response.model';
import { UsersService } from '../../../users/services/users.service';
import { DeleteOrderDialogComponent } from '../../components/delete-order-dialog/delete-order-dialog.component';
import { AirConditionerResponse } from '../../models/air-conditioner-response.model';
import { AirConditionersService } from '../../services/air-conditioners.service';
import { OrdersService } from '../../services/orders.service';

@Component({
	selector: 'app-orders-page',
	templateUrl: './orders-page.component.html',
	styleUrls: ['./orders-page.component.scss'],
})
export class OrdersPageComponent implements OnInit {
	allClients: UserResponse[] = [];
	allAirConditioners: AirConditionerResponse[] = [];
	ordersIds: string[] = [];

	constructor(
		private snackBar: MatSnackBar,
		private ordersService: OrdersService,
		private usersService: UsersService,
		private airConditionersService: AirConditionersService,
		private router: Router,
		private dialog: MatDialog
	) {}

	ngOnInit(): void {
		let clientsIds: string[] = [];
		let airConditionersIds: string[] = [];

		this.ordersService
			.getAll()
			.pipe(
				tap((response) =>
					response.forEach((x) => {
						clientsIds.push(x.clientId);
						airConditionersIds.push(x.airConditionerId);
						this.ordersIds.push(x.id);
					})
				),
				map((res) => {
					return {
						clients: this.fetchClients(clientsIds),
						airConditioners:
							this.fetchAirConditioners(airConditionersIds),
					};
				}),
				switchMap((response) =>
					forkJoin([response.clients, response.airConditioners])
				)
			)
			.subscribe((response) => {
				this.allClients = response[0];
				this.allAirConditioners = response[1];
			});
	}

	fetchClients(ids: string[]): Observable<UserResponse[]> {
		let clients: Observable<UserResponse>[] = [];
		ids.forEach((id) => clients.push(this.usersService.getUserById(id)));

		return forkJoin(clients);
	}

	fetchAirConditioners(ids: string[]): Observable<AirConditionerResponse[]> {
		let airConditioners: Observable<AirConditionerResponse>[] = [];

		ids.forEach((id) =>
			airConditioners.push(this.airConditionersService.getById(id))
		);

		return forkJoin(airConditioners);
	}

	goToUserDetails(id: string) {
		this.router.navigate([`/main/users/${id}`]);
	}

	goToAirConditionerDetails(id: string) {
		this.router.navigate([`/main/air-conditioners/${id}`]);
	}

	openDeleteOrderDialog(id: string) {
		const dialogRef = this.dialog.open(DeleteOrderDialogComponent, {
			data: id.toString(),
			autoFocus: false,
			panelClass: 'round-without-padding',
		});

		dialogRef
			.afterClosed()
			.pipe(take(1))
			.subscribe((orderId: string) => {
				const index = this.ordersIds.indexOf(orderId);

				if (index > -1) {
					this.ordersIds.splice(index, 1);
					this.allClients.splice(index, 1);
					this.allAirConditioners.splice(index, 1);
				}

				this.snackBar.open(
					'The order was deleted successfully',
					'Close'
				);
			});
	}
}
