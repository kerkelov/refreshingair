// Builtin
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// env
import { environment } from '../../../../environments/environment';

// Types etc
import { BehaviorSubject, map, Observable } from 'rxjs';
import { removeFalsyFromFilter } from 'src/app/core/utils/remove-falsy-from-filter';
import { AirConditionerType } from '../../../core/enums/air-conditioner-type';
import { AirConditionerFilter } from '../models/air-conditioner-filter.model';
import { AirConditionerPagableResponse } from '../models/air-conditioner-pagable-response.model';
import { AirConditionerRequest } from '../models/air-conditioner-request.model';
import { AirConditionerResponse } from '../models/air-conditioner-response.model';

@Injectable({
	providedIn: 'root',
})
export class AirConditionersService {
	airConditionerUpdatedObservable: BehaviorSubject<null> =
		new BehaviorSubject(null);
	airConditionersBackendUrl: string = `${environment.backendUrl}/AirConditioners`;
	ordersBackendUrl: string = `${environment.backendUrl}/AirConOrders`;

	constructor(private httpClient: HttpClient) {}

	private getNumberOfAirConType(type: AirConditionerType): number {
		console.log(type);
		if (type == AirConditionerType.Central) {
			return 0;
		} else if (type == AirConditionerType.Window) {
			return 1;
		} else if (type == AirConditionerType.Portable) {
			return 2;
		}
		return 100;
	}

	getAirConditioners(
		filter: AirConditionerFilter,
		page: number,
		size: number
	): Observable<AirConditionerPagableResponse> {
		let newFilter = removeFalsyFromFilter({
			...filter,
		});

		return this.httpClient
			.get<AirConditionerResponse[]>(`${this.airConditionersBackendUrl}`)
			.pipe(
				map((res) => {
					for (const key in newFilter) {
						res = res.filter((user) =>
							//@ts-ignore
							user[key]
								.toLowerCase()
								.includes(
									newFilter[key].toString().toLowerCase()
								)
						);

						res = res.sort((a) => {
							//@ts-ignore
							return a[key]
								.toLowerCase()
								.startsWith(
									newFilter[key].toString().toLowerCase()
								)
								? -1
								: 1;
						});
					}

					return {
						content: res,
						size,
						page,
					} as AirConditionerPagableResponse;
				})
			);
	}

	getById(id: string): Observable<AirConditionerResponse> {
		return this.httpClient
			.get<AirConditionerResponse>(
				`${this.airConditionersBackendUrl}/${id}`
			)
			.pipe(
				map((x) => {
					let result = AirConditionerType.Central;
					//@ts-ignore
					if (x.type == 1) {
						result = AirConditionerType.Window;
					}
					//@ts-ignore
					else if (x.type == 2) {
						result = AirConditionerType.Portable;
					}
					return {
						...x,
						airConditionerType: result,
					};
				})
			);
	}

	delete(id: string): Observable<void> {
		return this.httpClient.delete<void>(
			`${this.airConditionersBackendUrl}/${id}`
		);
	}

	create(
		airConditionerToBeCreated: AirConditionerRequest
	): Observable<AirConditionerResponse> {
		return this.httpClient.post<AirConditionerResponse>(
			`${this.airConditionersBackendUrl}`,
			{
				...airConditionerToBeCreated,
				type: this.getNumberOfAirConType(
					airConditionerToBeCreated.airConditionerType
				),
			}
		);
	}

	update(
		id: string,
		dataToBeUpdated: AirConditionerResponse
	): Observable<AirConditionerResponse> {
		return this.httpClient.put<AirConditionerResponse>(
			`${this.airConditionersBackendUrl}/${id}`,
			{
				...dataToBeUpdated,
				type: this.getNumberOfAirConType(
					dataToBeUpdated.airConditionerType
				),
			}
		);
	}
}
