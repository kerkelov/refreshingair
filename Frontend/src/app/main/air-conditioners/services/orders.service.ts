// Builtin
import { Injectable } from '@angular/core';

// env
import { environment } from '../../../../environments/environment';

// Types etc
import { HttpClient } from '@angular/common/http';
import { AirConOrderResponse } from '../models/air-con-order-response.model';
@Injectable({
	providedIn: 'root',
})
export class OrdersService {
	ordersBackendUrl: string = `${environment.backendUrl}/AirConOrders`;

	constructor(private httpClient: HttpClient) {}

	getAll() {
		return this.httpClient.get<AirConOrderResponse[]>(
			`${this.ordersBackendUrl}`
		);
	}

	create(clientId: string, airConditionerId: string) {
		return this.httpClient.post<string>(`${this.ordersBackendUrl}`, {
			clientId,
			airConditionerId,
		});
	}

	delete(orderId: string) {
		return this.httpClient.delete<void>(
			`${this.ordersBackendUrl}/${orderId}`
		);
	}
}
