import { TestBed } from '@angular/core/testing';

import { AirConditionersService } from './air-conditioners.service';

describe('AirConditionersService', () => {
	let service: AirConditionersService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(AirConditionersService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});
