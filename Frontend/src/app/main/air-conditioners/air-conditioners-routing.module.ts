import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AirConditionerDetailsComponent } from './pages/air-conditioners-details/air-conditioners-details.component';
import { AirConditionersPageComponent } from './pages/air-conditioners-page/air-conditioners-page.component';

const routes: Routes = [
	{
		path: '',
		component: AirConditionersPageComponent,
		pathMatch: 'full',
	},
	{
		path: ':id',
		component: AirConditionerDetailsComponent,
		data: {
			title: 'air conditioners',
			breadcrumb: [
				{
					label: 'Air Conditioners',
					url: 'air-conditioners',
				},
				{
					label: 'Air conditioner details',
					url: 'air-conditioners/:id',
				},
			],
		},
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AirConditionersRoutingModule {}
