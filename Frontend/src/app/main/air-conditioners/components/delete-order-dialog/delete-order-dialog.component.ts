import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { getErrorMessage } from '../../../../core/utils/get-error-message';
import { OrdersService } from '../../services/orders.service';

@Component({
	selector: 'app-delete-order-dialog',
	templateUrl: './delete-order-dialog.component.html',
	styleUrls: ['./delete-order-dialog.component.scss'],
})
export class DeleteOrderDialogComponent {
	constructor(
		@Inject(MAT_DIALOG_DATA)
		public id: string,
		private dialogRef: MatDialogRef<DeleteOrderDialogComponent>,
		private ordersService: OrdersService,
		private snackBar: MatSnackBar,
		private router: Router
	) {}

	deleteOrder(): void {
		const observer = {
			next: () => {
				this.dialogRef.close(this.id);
				this.router.navigate(['main/orders']);
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(getErrorMessage(httpError), 'Close');
			},
		};

		this.ordersService.delete(this.id).pipe(take(1)).subscribe(observer);
	}

	closeDialog(): void {
		this.dialogRef.close();
	}
}
