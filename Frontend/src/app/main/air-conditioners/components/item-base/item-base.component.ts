import {
	Directive,
	EventEmitter,
	Input,
	OnDestroy,
	Output,
} from '@angular/core';
import { Subject, take, takeUntil } from 'rxjs';
//Material
import { MatDialog } from '@angular/material/dialog';
//Services
import { CurrentUserService } from '../../../../core/services/current-user.service';
//Components
import { CreateAirConditionerFormComponent } from '../create-air-conditioner-form/create-air-conditioner-form.component';
import { DeleteAirConditionerDialogComponent } from '../delete-air-conditioner-dialog/delete-air-conditioner-dialog.component';
//Other
import { AirConditionerResponse } from '../../models/air-conditioner-response.model';
import { AirConditionersService } from '../../services/air-conditioners.service';
import { ConfirmOrderDialogComponent } from '../confirm-order-dialog/confirm-order-dialog.component';

@Directive()
export abstract class ItemBase implements OnDestroy {
	@Input() airConditioner!: AirConditionerResponse;
	@Input() isExtendedVersion!: boolean;
	@Output() delete = new EventEmitter();
	destroy$: Subject<void> = new Subject();

	constructor(
		public dialog: MatDialog,
		public currentUserService: CurrentUserService,
		public airConditionersService: AirConditionersService
	) {}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.unsubscribe();
	}

	openUpdateAirConditionerDialog(): void {
		const dialogRef = this.dialog.open(CreateAirConditionerFormComponent, {
			width: '60vw',
			autoFocus: false,
			disableClose: true,
			panelClass: ['round-without-padding', 'modal-container'],
			data: {
				airConditionerId: this.airConditioner.id,
			},
		});

		dialogRef
			.afterClosed()
			.pipe(take(1))
			.subscribe(() => {
				return this.airConditionersService.airConditionerUpdatedObservable.next(
					null
				);
			});
	}

	openDeleteAirConditionerDialog(id: string): void {
		const dialogRef = this.dialog.open(
			DeleteAirConditionerDialogComponent,
			{
				data: id.toString(),
				autoFocus: false,
				panelClass: 'round-without-padding',
			}
		);

		dialogRef
			.afterClosed()
			.pipe(takeUntil(this.destroy$))
			.subscribe(() => {
				this.delete.emit();
			});
	}

	openConfirmOrderDialog(id: string): void {
		const dialogRef = this.dialog.open(ConfirmOrderDialogComponent, {
			data: id.toString(),
			autoFocus: false,
			panelClass: 'round-without-padding',
		});
	}
}
