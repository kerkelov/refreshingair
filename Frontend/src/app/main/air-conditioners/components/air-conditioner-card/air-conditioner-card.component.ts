import { Component } from '@angular/core';
//Services
import { CurrentUserService } from '../../../../core/services/current-user.service';
//Material
import { MatDialog } from '@angular/material/dialog';

//Other
import { AirConditionersService } from '../../services/air-conditioners.service';
import { ItemBase } from '../item-base/item-base.component';

@Component({
	selector: 'app-air-conditioner-card',
	templateUrl: './air-conditioner-card.component.html',
	styleUrls: [
		'./air-conditioner-card.component.scss',
		'../../components/item-base/item-base.components.scss',
	],
})
export class AirConditionerCardComponent extends ItemBase {
	constructor(
		public override dialog: MatDialog,
		public override currentUserService: CurrentUserService,
		public override airConditionersService: AirConditionersService
	) {
		super(dialog, currentUserService, airConditionersService);
	}
}
