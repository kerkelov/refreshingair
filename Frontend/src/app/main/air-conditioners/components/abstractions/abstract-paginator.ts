import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, Subject, switchMap, takeUntil } from 'rxjs';

@Component({ template: '' })
// eslint-disable-next-line @angular-eslint/component-class-suffix
export abstract class AbstractPaginator<T> implements OnDestroy {
	@ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
	destroy$: Subject<void> = new Subject();
	size: number = 12;
	page: number = 0;
	count!: number;

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.unsubscribe();
	}

	getRangeDisplayText(
		page: number,
		pageSize: number,
		length: number
	): string {
		const numOfPages = Math.ceil(length / pageSize);
		return `Page ${page + 1} of ${numOfPages}`;
	}

	subscribeToPaginatorChanges(): Observable<T> {
		this.paginator._intl.getRangeLabel = this.getRangeDisplayText;
		return this.paginator.page.pipe(
			switchMap(() => {
				this.size = this.paginator.pageSize;
				this.page = this.paginator.pageIndex;

				return this.getData(this.page, this.size);
			}),
			takeUntil(this.destroy$)
		);
	}

	abstract getData(page: number, size: number): Observable<T>;
}
