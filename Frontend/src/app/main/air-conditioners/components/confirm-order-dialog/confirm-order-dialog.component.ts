import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs';
import { CurrentUserService } from '../../../../core/services/current-user.service';
import { getErrorMessage } from '../../../../core/utils/get-error-message';
import { OrdersService } from '../../services/orders.service';

@Component({
	selector: 'app-confirm-order-dialog',
	templateUrl: './confirm-order-dialog.component.html',
	styleUrls: ['./confirm-order-dialog.component.scss'],
})
export class ConfirmOrderDialogComponent {
	constructor(
		@Inject(MAT_DIALOG_DATA)
		public id: string,
		private dialogRef: MatDialogRef<ConfirmOrderDialogComponent>,
		private snackBar: MatSnackBar,
		private ordersService: OrdersService,
		private currentUserService: CurrentUserService
	) {}

	createOrder(): void {
		const observer = {
			next: (orderId: string) => {
				this.snackBar.open(
					'Your order was successfull, we will contact you',
					'Close'
				);
				this.dialogRef.close(orderId);
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(getErrorMessage(httpError), 'Close');
			},
		};

		this.ordersService
			.create(this.currentUserService.currentUser?.id!, this.id)
			.pipe(take(1))
			.subscribe(observer);
	}

	closeDialog(): void {
		this.dialogRef.close();
	}
}
