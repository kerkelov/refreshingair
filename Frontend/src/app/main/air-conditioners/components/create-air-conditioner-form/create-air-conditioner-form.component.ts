// Builtin
import { HttpErrorResponse } from '@angular/common/http';
import {
	Component,
	Inject,
	OnDestroy,
	OnInit,
	ViewEncapsulation,
} from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from '@angular/forms';

// Material
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

//Services
import { CurrentUserService } from '../../../../core/services/current-user.service';
import { AirConditionersService } from '../../services/air-conditioners.service';

// Types etc
import { finalize, Subject, takeUntil } from 'rxjs';
import { blankProfilePictureUrl } from '../../../../core/constants/urls';
import { AirConditionerType } from '../../../../core/enums/air-conditioner-type';
import { getErrorMessage } from '../../../../core/utils/get-error-message';
import { AirConditionerResponse } from '../../models/air-conditioner-response.model';

@Component({
	selector: 'app-create-air-conditioner-form',
	templateUrl: './create-air-conditioner-form.component.html',
	styleUrls: ['./create-air-conditioner-form.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class CreateAirConditionerFormComponent implements OnInit, OnDestroy {
	createAirConditionerForm!: FormGroup;
	types: string[] = ['Portable', 'Window', 'Central'];
	destroy$: Subject<void> = new Subject();
	title: string = 'Create air conditioner';
	loading: boolean = false;
	isUpdate: boolean = false;
	showError: boolean = false;

	constructor(
		private formBuilder: FormBuilder,
		private airConditionersService: AirConditionersService,
		private snackBar: MatSnackBar,
		@Inject(MAT_DIALOG_DATA) private data: { airConditionerId?: number },
		private dialogRef: MatDialogRef<CreateAirConditionerFormComponent>,
		public currentUserService: CurrentUserService
	) {}

	get pictureUrlControl(): AbstractControl | null {
		return this.createAirConditionerForm.get('pictureUrl');
	}

	get manufacturerControl(): AbstractControl | null {
		return this.createAirConditionerForm.get('manufacturer');
	}

	get descriptionControl(): AbstractControl | null {
		return this.createAirConditionerForm.get('description');
	}

	get modelControl(): AbstractControl | null {
		return this.createAirConditionerForm.get('model');
	}
	get countryControl(): AbstractControl | null {
		return this.createAirConditionerForm.get('country');
	}

	get priceControl(): AbstractControl | null {
		return this.createAirConditionerForm.get('price');
	}

	get typeControl(): AbstractControl | null {
		return this.createAirConditionerForm.get('airConditionerType');
	}

	ngOnInit(): void {
		this.setupTheAirConditionerForm();
	}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.unsubscribe();
	}

	onSubmit(): void {
		this.loading = true;

		if (this.data?.airConditionerId) {
			this.updateAirConditioner(true);
		} else {
			this.createAirConditioner();
		}
	}

	onNewPhotoSelected(newPhotoUrl: string): void {
		this.pictureUrlControl?.setValue(newPhotoUrl);
	}

	createAirConditioner(): void {
		const observer = {
			next: (airConditioner: AirConditionerResponse) => {
				this.dialogRef.close(airConditioner);

				this.snackBar.open(
					`Successfully created air conditioner.`,
					'Ok'
				);
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(getErrorMessage(httpError), 'Close');
			},
		};

		this.airConditionersService
			.create(this.createAirConditionerForm.value)
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => (this.loading = false))
			)
			.subscribe(observer);
	}

	updateAirConditioner(isFinalUpdate: boolean): void {
		const observer = {
			next: (airConditioner: AirConditionerResponse) => {
				if (isFinalUpdate) {
					this.snackBar.open(
						`Successfully updated air conditioner.`,
						'Ok'
					);
					this.dialogRef.close(airConditioner);
				} else {
					this.updateAirConditionerForm(airConditioner);
				}
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(getErrorMessage(httpError), 'Close');
			},
		};

		const dataToBeUpdated = {
			...this.createAirConditionerForm.value,
			id: this.data.airConditionerId,
		};

		this.airConditionersService
			.update(String(this.data.airConditionerId!), dataToBeUpdated)
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => (this.loading = false))
			)
			.subscribe(observer);
	}

	setupTheAirConditionerForm(): void {
		this.createAirConditionerForm = this.formBuilder.group({
			pictureUrl: new FormControl(blankProfilePictureUrl),
			manufacturer: new FormControl('', Validators.required),
			country: new FormControl('', Validators.required),
			price: new FormControl('', Validators.required),
			description: new FormControl('', []),
			model: new FormControl('', [Validators.required]),
			airConditionerType: new FormControl(AirConditionerType.Central, [
				Validators.required,
			]),
		});

		if (this.data?.airConditionerId) {
			this.title = 'Update air conditioner';
			this.isUpdate = true;
			this.airConditionersService
				.getById(String(this.data.airConditionerId))
				.pipe(takeUntil(this.destroy$))
				.subscribe((airConditioner) => {
					this.updateAirConditionerForm(airConditioner);
				});
		}
	}

	updateAirConditionerForm(airConditioner: AirConditionerResponse): void {
		console.log(airConditioner);
		this.createAirConditionerForm.setValue({
			pictureUrl: airConditioner.pictureUrl,
			model: airConditioner.model,
			country: airConditioner.country,
			price: airConditioner.price,
			manufacturer: airConditioner.manufacturer,
			description: airConditioner.description,
			airConditionerType: airConditioner.airConditionerType,
		});
	}
}
