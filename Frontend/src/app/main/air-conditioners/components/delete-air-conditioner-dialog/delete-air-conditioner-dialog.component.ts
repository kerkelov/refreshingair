import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { getErrorMessage } from '../../../../core/utils/get-error-message';
import { AirConditionersService } from '../../services/air-conditioners.service';

@Component({
	selector: 'app-delete-air-conditioner-dialog',
	templateUrl: './delete-air-conditioner-dialog.component.html',
	styleUrls: [
		'./delete-air-conditioner-dialog.component.scss',
		'../../../../../assets/styles/gradient-button.scss',
	],
})
export class DeleteAirConditionerDialogComponent {
	constructor(
		@Inject(MAT_DIALOG_DATA)
		public id: string,
		private dialogRef: MatDialogRef<DeleteAirConditionerDialogComponent>,
		private airConditionersService: AirConditionersService,
		private snackBar: MatSnackBar,
		private router: Router
	) {}

	deleteAirConditioner(): void {
		const observer = {
			next: () => {
				this.router.navigate(['main/air-conditioners']);
				this.dialogRef.close(this.id);
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(getErrorMessage(httpError), 'Close');
			},
		};

		this.airConditionersService
			.delete(this.id)
			.pipe(take(1))
			.subscribe(observer);
	}

	closeDialog(): void {
		this.dialogRef.close();
	}
}
