import { Component } from '@angular/core';
//Material
import { MatDialog } from '@angular/material/dialog';
//Services
import { CurrentUserService } from '../../../../core/services/current-user.service';

//Other
import { AirConditionersService } from '../../services/air-conditioners.service';
import { ItemBase } from '../item-base/item-base.component';

@Component({
	selector: 'app-list-item',
	templateUrl: './list-item.component.html',
	styleUrls: [
		'./list-item.component.scss',
		'../../components/item-base/item-base.components.scss',
	],
})
export class ListItemComponent extends ItemBase {
	constructor(
		public override dialog: MatDialog,
		public override currentUserService: CurrentUserService,
		public override airConditionersService: AirConditionersService
	) {
		super(dialog, currentUserService, airConditionersService);
	}
}
