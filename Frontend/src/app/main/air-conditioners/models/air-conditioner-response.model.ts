import { AirConditionerType } from '../../../core/enums/air-conditioner-type';

export interface AirConditionerResponse {
	id: string;
	model: string;
	country: string;
	manufacturer: string;
	pictureUrl: string;
	description: string;
	price: number;
	airConditionerType: AirConditionerType;
}
