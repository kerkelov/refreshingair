export interface AirConOrderRequest {
	clientId: string;
	airConditionerId: string;
}
