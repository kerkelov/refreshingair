import { AirConditionerType } from '../../../core/enums/air-conditioner-type';

export interface AirConditionerRequest {
	model: string;
	country: string;
	manifacturer: string;
	pictureUrl: string;
	description: string;
	price: number;
	airConditionerType: AirConditionerType;
}
