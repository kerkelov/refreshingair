export interface AirConOrderResponse {
	id: string;
	clientId: string;
	airConditionerId: string;
}
