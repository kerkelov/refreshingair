import { AirConditionerResponse } from './air-conditioner-response.model';

export interface AirConditionerPagableResponse {
	content: AirConditionerResponse[];
	page: number;
	size: number;
}
