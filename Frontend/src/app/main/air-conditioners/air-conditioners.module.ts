import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AirConditionersRoutingModule } from './air-conditioners-routing.module';
import { AirConditionerCardComponent } from './components/air-conditioner-card/air-conditioner-card.component';
import { ConfirmOrderDialogComponent } from './components/confirm-order-dialog/confirm-order-dialog.component';
import { CreateAirConditionerFormComponent } from './components/create-air-conditioner-form/create-air-conditioner-form.component';
import { DeleteAirConditionerDialogComponent } from './components/delete-air-conditioner-dialog/delete-air-conditioner-dialog.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { AirConditionerDetailsComponent } from './pages/air-conditioners-details/air-conditioners-details.component';
import { AirConditionersPageComponent } from './pages/air-conditioners-page/air-conditioners-page.component';
import { OrdersPageComponent } from './pages/orders-page/orders-page.component';
import { DeleteOrderDialogComponent } from './components/delete-order-dialog/delete-order-dialog.component';
@NgModule({
	declarations: [
		CreateAirConditionerFormComponent,
		AirConditionerCardComponent,
		AirConditionersPageComponent,
		AirConditionerDetailsComponent,
		DeleteAirConditionerDialogComponent,
		ListItemComponent,
		ConfirmOrderDialogComponent,
		OrdersPageComponent,
  DeleteOrderDialogComponent,
	],
	imports: [
		// Builtin
		CommonModule,
		RouterModule,
		ReactiveFormsModule,

		//Routing
		AirConditionersRoutingModule,

		//Modules
		SharedModule,
		FormsModule,

		//Material
		MatFormFieldModule,
		MatInputModule,
		MatDialogModule,
		MatButtonModule,
		MatIconModule,
		MatSnackBarModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatRadioModule,
		MatSelectModule,
		MatButtonToggleModule,
		MatPaginatorModule,
		MatButtonToggleModule,
	],
})
export class AirConditionersModule {}
