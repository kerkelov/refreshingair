import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersPageComponent } from './air-conditioners/pages/orders-page/orders-page.component';
import { MainComponent } from './main.component';

const routes: Routes = [
	{
		path: '',
		component: MainComponent,
		children: [
			{
				path: '',
				redirectTo: 'users',
				pathMatch: 'full',
			},
			{
				path: 'users',
				loadChildren: () =>
					import('./users/users.module').then((m) => m.UsersModule),
				data: {
					title: 'users',
					breadcrumb: [
						{
							label: 'Users',
							url: '/users',
						},
					],
				},
			},
			{
				path: 'air-conditioners',
				loadChildren: () =>
					import('./air-conditioners/air-conditioners.module').then(
						(m) => m.AirConditionersModule
					),
				data: {
					title: 'air conditioners',
					breadcrumb: [
						{
							label: 'Air conditioners',
							url: '/air-conditioners',
						},
					],
				},
			},
			{
				path: 'orders',
				component: OrdersPageComponent,
				data: {
					title: 'orders',
					breadcrumb: [
						{
							label: 'Orders',
							url: '/orders',
						},
					],
				},
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class MainRoutingModule {}
