import { Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
	selector: 'app-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnDestroy {
	isOpen: boolean = false;

	constructor(private dialogRef: MatDialog) {}

	ngOnDestroy(): void {
		this.dialogRef.closeAll();
	}

	onNavigationToggle(): void {
		this.isOpen = !this.isOpen;
	}

	changePage(): void {
		const width =
			window.innerWidth ||
			document.documentElement.clientWidth ||
			document.body.clientWidth;

		if (width <= 900) {
			this.isOpen = false;
		}
	}
}
