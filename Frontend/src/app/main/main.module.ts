// Builtin
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { MainRoutingModule } from './main-routing.module';

// Modules
import { LayoutModule } from './layout/layout.module';
import {NgDynamicBreadcrumbModule} from "ng-dynamic-breadcrumb";

//Componenets
import { MainComponent } from './main.component';

@NgModule({
	declarations: [MainComponent],
	imports: [
		// Builtin
		CommonModule,
		// Routing
		MainRoutingModule,
		// Modules
		LayoutModule,
		NgDynamicBreadcrumbModule
	],
})
export class MainModule {}
