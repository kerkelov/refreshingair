export interface UserFilter {
	firstName?: string;
	lastName?: string;
	phoneNumber?: string;
	email?: string;
}
