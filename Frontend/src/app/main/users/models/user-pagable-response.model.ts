import { UserResponse } from '../../../core/models/user-response.model';

// Fully copied from swagger. Some attributes seem questionable
// but better to match then to be sorry.

export interface UserPagableResponse {
	content: UserResponse[];
	page: number;
	size: number;
}
