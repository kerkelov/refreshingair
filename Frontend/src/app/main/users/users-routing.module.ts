import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from '../../core/guards/admin.guard';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';

const routes: Routes = [
	{
		path: '',
		component: UsersPageComponent,
		pathMatch: 'full',
		canActivate: [AdminGuard],
	},
	{
		path: ':id',
		component: UserProfileComponent,
		data: {
			title: 'users profile',
			breadcrumb: [
				{
					label: 'Users',
					url: 'users',
				},
				{
					label: 'Users Profile',
					url: 'users/:id',
				},
			],
		},
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class UsersRoutingModule {}
