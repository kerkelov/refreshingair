import { Component } from '@angular/core';
//Material
import { MatDialog } from '@angular/material/dialog';
//Services
import { CurrentUserService } from '../../../../core/services/current-user.service';

//Other
import { UsersService } from '../../services/users.service';
import { UserItemBase } from '../users-item-base/user-item-base.component';

@Component({
	selector: 'app-users-list-item',
	templateUrl: './users-list-item.component.html',
	styleUrls: [
		'./users-list-item.component.scss',
		'../../components/users-item-base/user-item-base.components.scss',
	],
})
export class UsersListItemComponent extends UserItemBase {
	constructor(
		public override dialog: MatDialog,
		public override currentUserService: CurrentUserService,
		public override usersService: UsersService
	) {
		super(dialog, currentUserService, usersService);
	}
}
