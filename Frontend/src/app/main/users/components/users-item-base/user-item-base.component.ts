import {
	Directive,
	EventEmitter,
	Input,
	OnDestroy,
	Output,
} from '@angular/core';
import { Subject, take, takeUntil } from 'rxjs';
//Material
import { MatDialog } from '@angular/material/dialog';
//Services
import { CurrentUserService } from '../../../../core/services/current-user.service';
//Components
import { CreateUserFormComponent } from '../create-user-form/create-user-form.component';
import { DeleteUserDialogComponent } from '../delete-user-dialog/delete-user-dialog.component';
//Other
import { UserResponse } from '../../../../core/models/user-response.model';
import { UsersService } from '../../services/users.service';

@Directive()
export abstract class UserItemBase implements OnDestroy {
	@Input() user!: UserResponse;
	@Input() isExtendedVersion!: boolean;
	@Output() deleteUser = new EventEmitter();
	destroy$: Subject<void> = new Subject();

	constructor(
		public dialog: MatDialog,
		public currentUserService: CurrentUserService,
		public usersService: UsersService
	) {}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.unsubscribe();
	}

	openUpdateUserDialog(): void {
		const dialogRef = this.dialog.open(CreateUserFormComponent, {
			width: '60vw',
			autoFocus: false,
			disableClose: true,
			panelClass: ['round-without-padding', 'modal-container'],
			data: {
				userId: this.user.id,
			},
		});

		dialogRef
			.afterClosed()
			.pipe(take(1))
			.subscribe(() => {
				return this.usersService.userUpdatedObservable.next(null);
			});
	}

	openDeleteUserDialog(id: string): void {
		const dialogRef = this.dialog.open(DeleteUserDialogComponent, {
			data: id.toString(),
			autoFocus: false,
			panelClass: 'round-without-padding',
		});

		dialogRef
			.afterClosed()
			.pipe(takeUntil(this.destroy$))
			.subscribe(() => {
				this.deleteUser.emit();
			});
	}
}
