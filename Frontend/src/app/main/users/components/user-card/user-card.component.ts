import { Component } from '@angular/core';
//Services
import { CurrentUserService } from '../../../../core/services/current-user.service';
//Material
import { MatDialog } from '@angular/material/dialog';

//Other
import { UsersService } from '../../services/users.service';
import { UserItemBase } from '../users-item-base/user-item-base.component';

@Component({
	selector: 'app-user-card',
	templateUrl: './user-card.component.html',
	styleUrls: [
		'./user-card.component.scss',
		'../../components/users-item-base/user-item-base.components.scss',
	],
})
export class UserCardComponent extends UserItemBase {
	constructor(
		public override dialog: MatDialog,
		public override currentUserService: CurrentUserService,
		public override usersService: UsersService
	) {
		super(dialog, currentUserService, usersService);
	}
}
