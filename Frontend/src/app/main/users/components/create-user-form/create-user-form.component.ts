// Builtin
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {
	Component,
	Inject,
	OnDestroy,
	OnInit,
	ViewEncapsulation,
} from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from '@angular/forms';

// Material
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

//Services
import { CurrentUserService } from '../../../../core/services/current-user.service';
import { UsersService } from '../../services/users.service';

// Types etc
import { catchError, finalize, Subject, takeUntil } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { blankProfilePictureUrl } from '../../../../core/constants/urls';
import { Role } from '../../../../core/enums/role';
import { UserRequest } from '../../../../core/models/user-request.model';
import { UserResponse } from '../../../../core/models/user-response.model';
import { getErrorMessage } from '../../../../core/utils/get-error-message';

@Component({
	selector: 'app-create-user-form',
	templateUrl: './create-user-form.component.html',
	styleUrls: ['./create-user-form.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class CreateUserFormComponent implements OnInit, OnDestroy {
	createUserForm!: FormGroup;
	roles: string[] = ['Client', 'Technician', 'Admin'];
	destroy$: Subject<void> = new Subject();
	title: string = 'Create user';
	loading: boolean = false;
	isUpdate: boolean = false;
	showError: boolean = false;
	isTechnician: boolean = false;
	techniciansBackendUrl: string = `${environment.backendUrl}/Technicians`;

	constructor(
		private formBuilder: FormBuilder,
		private usersService: UsersService,
		private snackBar: MatSnackBar,
		@Inject(MAT_DIALOG_DATA) private data: { userId?: number },
		private dialogRef: MatDialogRef<CreateUserFormComponent>,
		public currentUserService: CurrentUserService,
		private httpClient: HttpClient
	) {}

	get profilePictureControl(): AbstractControl | null {
		return this.createUserForm.get('profilePictureUrl');
	}

	get firstNameControl(): AbstractControl | null {
		return this.createUserForm.get('firstName');
	}

	get lastNameControl(): AbstractControl | null {
		return this.createUserForm.get('lastName');
	}

	get userNameControl(): AbstractControl | null {
		return this.createUserForm.get('userName');
	}

	get emailControl(): AbstractControl | null {
		return this.createUserForm.get('email');
	}

	get phoneNumberControl(): AbstractControl | null {
		return this.createUserForm.get('phoneNumber');
	}

	get roleControl(): AbstractControl | null {
		return this.createUserForm.get('role');
	}

	get locationControl(): AbstractControl | null {
		return this.createUserForm.get('location');
	}

	ngOnInit(): void {
		this.setupTheCreateUserForm();
	}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.unsubscribe();
	}

	onSubmit(): void {
		this.loading = true;

		if (this.data?.userId) {
			this.updateUser(true);
		} else {
			this.createUser();
		}
	}

	onNewPhotoSelected(newPhotoUrl: string): void {
		this.profilePictureControl?.setValue(newPhotoUrl);
	}

	createUser(): void {
		const observer = {
			next: (user: UserResponse) => {
				this.dialogRef.close(user);

				this.snackBar.open(
					`Successfully created a user. Password is his names connected plus 1!.`,
					'Ok'
				);
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(getErrorMessage(httpError), 'Close');
			},
		};

		var userName =
			this.firstNameControl?.value + this.lastNameControl?.value;

		let userRequest: UserRequest = {
			...this.createUserForm.value,
			password: userName + '1!',
			userName: this.userNameControl?.value || userName,
		};

		this.usersService
			.create(userRequest)
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => (this.loading = false))
			)
			.subscribe(observer);
	}

	updateUser(isFinalUpdate: boolean): void {
		const observer = {
			next: (user: UserResponse) => {
				if (isFinalUpdate) {
					this.snackBar.open(`Successfully updated user.`, 'Ok');
					this.dialogRef.close(user);
				} else {
					this.updateUserForm(user);
				}
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(getErrorMessage(httpError), 'Close');
			},
		};

		const dataToBeUpdated = {
			...this.createUserForm.value,
			id: this.data.userId,
		};

		this.usersService
			.updateUser(String(this.data.userId!), dataToBeUpdated)
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => (this.loading = false))
			)
			.pipe(
				catchError((err) => {
					if (this.roleControl?.value != Role.Technician) {
					}
					return this.httpClient.put<UserResponse>(
						`${this.techniciansBackendUrl}/${String(
							this.data.userId!
						)}`,
						dataToBeUpdated
					);
				})
			)
			.subscribe(observer);
	}

	setupTheCreateUserForm(): void {
		this.createUserForm = this.formBuilder.group({
			profilePictureUrl: new FormControl(blankProfilePictureUrl),
			userName: new FormControl(''),
			firstName: new FormControl('', Validators.required),
			lastName: new FormControl('', Validators.required),
			email: new FormControl('', [Validators.required, Validators.email]),
			phoneNumber: new FormControl('', [
				Validators.required,
				Validators.pattern('[- +()0-9]+'),
			]),
			role: new FormControl(Role.Client, [Validators.required]),
		});

		if (this.data?.userId) {
			this.title = 'Update user';
			this.isUpdate = true;
			this.usersService
				.getUserById(String(this.data.userId))
				.pipe(
					catchError((err) => {
						return this.httpClient.get<UserResponse>(
							`${this.techniciansBackendUrl}/${String(
								this.data.userId
							)}`
						);
					})
				)
				.pipe(takeUntil(this.destroy$))
				.subscribe((user) => {
					this.updateUserForm(user);
					this.addLocationControl(user.location);
				});
		}
	}

	updateUserForm(user: UserResponse): void {
		this.createUserForm.setValue({
			profilePictureUrl: user.profilePictureUrl,
			firstName: user.firstName,
			lastName: user.lastName,
			email: user.email,
			phoneNumber: user.phoneNumber,
			userName: user.userName,
			role: user.role == 'Administrator' ? 'Admin' : user.role,
		});
	}

	addLocationControl(technicianValue?: string): void {
		this.createUserForm.addControl(
			'location',
			new FormControl(technicianValue)
		);
		this.isTechnician = true;
	}

	removeLocationControl(): void {
		this.createUserForm.removeControl('location');
		this.isTechnician = false;
	}

	newRoleSelected(role: string): void {
		if (role == Role.Technician) {
			this.addLocationControl();
		} else {
			this.removeLocationControl();
		}
	}
}
