import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { CurrentUserService } from '../../../../core/services/current-user.service';
import { LocalStorageUtilService } from '../../../../core/services/local-storage-util.service';
import { getErrorMessage } from '../../../../core/utils/get-error-message';
import { UsersService } from '../../services/users.service';

@Component({
	selector: 'app-delete-user-dialog',
	templateUrl: './delete-user-dialog.component.html',
	styleUrls: [
		'./delete-user-dialog.component.scss',
		'../../../../../assets/styles/gradient-button.scss',
	],
})
export class DeleteUserDialogComponent {
	constructor(
		@Inject(MAT_DIALOG_DATA)
		public id: string,
		private dialogRef: MatDialogRef<DeleteUserDialogComponent>,
		private userService: UsersService,
		private snackBar: MatSnackBar,
		private currentUserService: CurrentUserService,
		private router: Router,
		private localStorage: LocalStorageUtilService
	) {}

	deleteUser(): void {
		const observer = {
			next: () => {
				if (this.currentUserService.currentUser?.id == this.id) {
					this.localStorage.clearStorage();
					this.currentUserService.clearCurrentUser();
					this.router.navigate(['/public']);
				}

				this.router.navigate(['main/users']);
				this.dialogRef.close(this.id);
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(getErrorMessage(httpError), 'Close');
			},
		};

		this.userService.deleteUser(this.id).pipe(take(1)).subscribe(observer);
	}

	closeDialog(): void {
		this.dialogRef.close();
	}
}
