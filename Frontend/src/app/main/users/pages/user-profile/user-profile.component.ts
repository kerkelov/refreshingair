//Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, catchError, takeUntil } from 'rxjs';
//Services
import { UsersService } from '../../services/users.service';
//Models
import { UserResponse } from '../../../../core/models/user-response.model';
//Material
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from '../../../../../environments/environment';

@Component({
	selector: 'app-user-profile',
	templateUrl: './user-profile.component.html',
	styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit, OnDestroy {
	user: UserResponse | null = null;
	id!: string;
	destroy$: Subject<void> = new Subject();
	techniciansBackendUrl: string = `${environment.backendUrl}/Technicians`;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		public usersService: UsersService,
		private snackBar: MatSnackBar,
		private titleService: Title, //TODO: add it // private ordersService: OrdersService
		private cdr: ChangeDetectorRef,
		private httpClient: HttpClient
	) {
		this.titleService.setTitle('Refreshing Air | Profile');
	}

	ngOnInit(): void {
		this.fetchUser();
		this.usersService.userUpdatedObservable
			.pipe(takeUntil(this.destroy$))
			.subscribe({
				next: () => this.fetchUser(),
			});
	}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.unsubscribe();
	}

	fetchUser(): void {
		this.id = this.route.snapshot.paramMap.get('id')!;
		this.usersService
			.getUserById(this.id)
			.pipe(
				catchError((err) => {
					return this.httpClient.get<UserResponse>(
						`${this.techniciansBackendUrl}/${this.id}`
					);
				})
			)
			.pipe(takeUntil(this.destroy$))
			.subscribe(
				(res) => {
					this.user = res;
				},
				() => {
					this.router.navigate(['main/users']);
					this.snackBar.open(`User not found!`, 'Close', {
						duration: 4000,
					});
				}
			);
	}

	onDeleteUser(user: UserResponse): void {
		this.user = null;
		this.snackBar.open(`User ${user.userName} is deleted`, 'Close');
		this.router.navigate(['/main/users']);
	}
}
