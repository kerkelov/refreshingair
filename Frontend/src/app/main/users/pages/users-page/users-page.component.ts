import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, finalize, switchMap, takeUntil } from 'rxjs';
import { Role } from '../../../../core/enums/role';
import { UserResponse } from '../../../../core/models/user-response.model';
import { CurrentUserService } from '../../../../core/services/current-user.service';
import { AbstractPaginator } from '../../components/abstractions/abstract-paginator';
import { CreateUserFormComponent } from '../../components/create-user-form/create-user-form.component';
import { UserPagableResponse } from '../../models/user-pagable-response.model';
import { UsersService } from '../../services/users.service';

@Component({
	selector: 'app-users',
	templateUrl: './users-page.component.html',
	styleUrls: [
		'./users-page.component.scss',
		'../../../../../assets/styles/gradient-button.scss',
	],
})
export class UsersPageComponent
	extends AbstractPaginator<UserPagableResponse>
	implements OnInit, AfterViewInit
{
	users: UserResponse[] = [];
	isLoading: boolean = false;
	filterForm!: FormGroup;
	roles: string[] = Object.values(Role);
	isListDisplayed!: boolean;

	constructor(
		private usersService: UsersService,
		private snackBar: MatSnackBar,
		private dialog: MatDialog,
		private formBuilder: FormBuilder,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private titleService: Title,
		public currentUserService: CurrentUserService
	) {
		super();
		this.titleService.setTitle('RefreshingAir | Users');
	}

	get firstNameControl(): AbstractControl | null {
		return this.filterForm.get('firstName');
	}

	get lastNameControl(): AbstractControl | null {
		return this.filterForm.get('lastName');
	}

	get emailControl(): AbstractControl | null {
		return this.filterForm.get('email');
	}

	get phoneNumberControl(): AbstractControl | null {
		return this.filterForm.get('phoneNumber');
	}

	ngOnInit(): void {
		this.activatedRoute.paramMap
			.pipe(
				switchMap((params) => {
					this.createFilterForm(params);
					return this.getData(this.page, this.size);
				}),
				takeUntil(this.destroy$)
			)
			.subscribe(this.getObserver());

		this.usersService.userUpdatedObservable.subscribe(() => {
			return this.getData(this.page, this.size).subscribe(
				this.getObserver()
			);
		});
	}

	ngAfterViewInit(): void {
		this.subscribeToPaginatorChanges()
			.pipe(takeUntil(this.destroy$))
			.subscribe(this.getObserver());
	}

	createFilterForm(params: ParamMap): void {
		this.filterForm = this.formBuilder.group({
			firstName: [params.get('fullName') || ''],
			lastName: [params.get('fullName') || ''],
			phoneNumber: [params.get('location') || ''],
			email: [params.get('location') || ''],
		});
	}

	applyFilters(reset?: boolean): void {
		this.paginator.pageIndex = 0;
		this.page = 0;

		this.phoneNumberControl?.setValue(
			(this.phoneNumberControl.value || '').trim()
		);
		this.firstNameControl?.setValue(
			(this.firstNameControl.value || '').trim()
		);
		this.lastNameControl?.setValue(
			(this.lastNameControl.value || '').trim()
		);

		if (reset) {
			this.filterForm.reset();
		}

		this.getData(this.page, this.size).subscribe(this.getObserver());
	}

	openCreateUserDialog(): void {
		const dialogRef = this.dialog.open(CreateUserFormComponent, {
			width: '60vw',
			autoFocus: false,
			disableClose: true,
			panelClass: ['round-without-padding', 'modal-container'],
		});

		dialogRef
			.afterClosed()
			.pipe(
				switchMap(() => this.getData(this.page, this.size)),
				takeUntil(this.destroy$)
			)
			.subscribe(this.getObserver());
	}

	onDeletedUser(user: UserResponse): void {
		this.getData(this.page, this.size).subscribe(this.getObserver());
		this.snackBar.open(`User ${user.userName} is deleted`, 'Close');
	}

	getObserver(): {
		next: (res: UserPagableResponse) => void;
		error: () => void;
	} {
		return {
			next: (res: UserPagableResponse) => {
				this.users = res.content;
				this.count = res.content.length;
			},
			error: () => {
				console.log('possibly error occured');
			},
		};
	}

	getData(page: number, size: number): Observable<UserPagableResponse> {
		this.isLoading = true;

		return this.usersService
			.getUsers(this.filterForm.value, page, size)
			.pipe(
				finalize(() => (this.isLoading = false)),
				takeUntil(this.destroy$)
			);
	}
}
