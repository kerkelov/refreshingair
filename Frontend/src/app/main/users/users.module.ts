// Builtin
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

//Components
import { CreateUserFormComponent } from './components/create-user-form/create-user-form.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { DeleteUserDialogComponent } from './components/delete-user-dialog/delete-user-dialog.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { UserCardComponent } from './components/user-card/user-card.component';
import { UsersListItemComponent } from './components/users-list-item/users-list-item.component';

//Modules
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { UsersRoutingModule } from './users-routing.module';

//Material
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

@NgModule({
	declarations: [
		UsersPageComponent,
		CreateUserFormComponent,
		UserCardComponent,
		UserProfileComponent,
		DeleteUserDialogComponent,
		UsersListItemComponent,
	],
	imports: [
		// Builtin
		CommonModule,
		RouterModule,
		ReactiveFormsModule,

		//Routing
		UsersRoutingModule,

		//Modules
		SharedModule,
		FormsModule,

		//Material
		MatFormFieldModule,
		MatInputModule,
		MatDialogModule,
		MatButtonModule,
		MatIconModule,
		MatSnackBarModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatRadioModule,
		MatSelectModule,
		MatButtonToggleModule,
		MatPaginatorModule,
		MatButtonToggleModule,
	],
})
export class UsersModule {}
