// Builtin
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// env
import { environment } from '../../../../environments/environment';

// Types etc
import { BehaviorSubject, catchError, forkJoin, map, Observable } from 'rxjs';
import { removeFalsyFromFilter } from 'src/app/core/utils/remove-falsy-from-filter';
import { UserRequest } from '../../../core/models/user-request.model';
import { UserResponse } from '../../../core/models/user-response.model';
import { UserFilter } from '../models/user-filter.model';
import { UserPagableResponse } from '../models/user-pagable-response.model';

@Injectable({
	providedIn: 'root',
})
export class UsersService {
	userUpdatedObservable: BehaviorSubject<null> = new BehaviorSubject(null);
	usersBackendUrl: string = `${environment.backendUrl}/Users`;
	techniciansBackendUrl: string = `${environment.backendUrl}/Technicians`;

	constructor(private httpClient: HttpClient) {}

	getUsers(
		userFilter: UserFilter,
		page: number,
		size: number
	): Observable<UserPagableResponse> {
		let newFilter = removeFalsyFromFilter({
			...userFilter,
		});

		return forkJoin(
			this.httpClient.get<UserResponse[]>(
				`${this.techniciansBackendUrl}`
			),
			this.httpClient.get<UserResponse[]>(`${this.usersBackendUrl}`)
		)
			.pipe(map((x) => x[0].concat(x[1])))
			.pipe(
				map((x) => {
					let tempMap = new Set();
					let result: UserResponse[] = [];
					x.forEach((user) => {
						if (!tempMap.has(user.email)) {
							tempMap.add(user.email);
							result.push(user);
						}
					});

					return result;
				})
			)
			.pipe(
				map((res) => {
					for (const key in newFilter) {
						res = res.filter((user) =>
							//@ts-ignore
							user[key]
								.toLowerCase()
								.includes(
									newFilter[key].toString().toLowerCase()
								)
						);

						res = res.sort((a) => {
							//@ts-ignore
							return a[key]
								.toLowerCase()
								.startsWith(
									newFilter[key].toString().toLowerCase()
								)
								? -1
								: 1;
						});
					}

					return {
						content: res,
						size,
						page,
					} as UserPagableResponse;
				})
			);
	}

	getUserById(id: string): Observable<UserResponse> {
		return this.httpClient
			.get<UserResponse>(`${this.usersBackendUrl}/${id}`)
			.pipe(
				catchError((err) => {
					return this.httpClient.get<UserResponse>(
						`${this.techniciansBackendUrl}/${id}`
					);
				})
			)
			.pipe(
				map((user: UserResponse) => {
					return {
						...user,
						createdOn: user.createdOn.split(' ')[0],
						role:
							user.role.toUpperCase() == 'USER'
								? 'Client'
								: user.role,
					};
				})
			);
	}

	deleteUser(id: string): Observable<void> {
		return this.httpClient
			.delete<void>(`${this.usersBackendUrl}/${id}`)
			.pipe(
				catchError((err) => {
					return this.httpClient.delete<void>(
						`${this.techniciansBackendUrl}/${id}`
					);
				})
			);
	}

	create(userToBeCreated: UserRequest): Observable<UserResponse> {
		if (userToBeCreated.role.toUpperCase() == 'ADMIN') {
			return this.httpClient.post<UserResponse>(
				`${this.usersBackendUrl}/admin`,
				userToBeCreated
			);
		} else if (userToBeCreated.role.toUpperCase() == 'TECHNICIAN') {
			return this.httpClient.post<UserResponse>(
				`${this.techniciansBackendUrl}`,
				userToBeCreated
			);
		}

		return this.httpClient.post<UserResponse>(
			`${this.usersBackendUrl}/client`,
			userToBeCreated
		);
	}

	updateUser(
		id: string,
		dataToBeUpdated: UserResponse
	): Observable<UserResponse> {
		return this.httpClient
			.put<UserResponse>(`${this.usersBackendUrl}/${id}`, dataToBeUpdated)
			.pipe(
				catchError((err) => {
					return this.httpClient.put<UserResponse>(
						`${this.techniciansBackendUrl}/${id}`,
						dataToBeUpdated
					);
				})
			);
	}

	changePassword(
		id: string,
		oldPassword: string,
		newPassword: string
	): Observable<null> {
		return this.httpClient.post<null>(`${this.usersBackendUrl}/password`, {
			id,
			oldPassword,
			newPassword,
		});
	}
}
