import { Component, EventEmitter, Input, Output } from '@angular/core';
import { sideNavItems } from 'src/app/core/constants/side-nav-items';
import { sideNavItem } from 'src/app/core/models/side-nav.model';
import { CurrentUserService } from '../../../core/services/current-user.service';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
	@Input() isOpen!: boolean;
	@Output() sideClick = new EventEmitter();
	sidenavItems: sideNavItem[] = sideNavItems;

	constructor(public currentUserService: CurrentUserService) {}

	changePage(): void {
		this.sideClick.emit();
	}
}
