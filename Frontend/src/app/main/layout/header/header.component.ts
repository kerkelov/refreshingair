// Builtin
import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';

// Material
import { MatDialog } from '@angular/material/dialog';

// Components
import { ChangePasswordFormComponent } from './modals/change-password-form/change-password-form.component';

// Services
import { CurrentUserService } from '../../../core/services/current-user.service';
import { LocalStorageUtilService } from '../../../core/services/local-storage-util.service';
import { ThemeService } from '../../../core/services/theme.service';

// Types etc
import { jwtTokenKey } from '../../../core/constants/local-storage-keys';
import { Theme } from '../../../core/enums/theme';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
	@Output() toggleSide = new EventEmitter();
	isLightMode: boolean;
	changeIcon: boolean = true;

	constructor(
		private router: Router,
		private localStorageUtil: LocalStorageUtilService,
		public currentUserService: CurrentUserService,
		public themeService: ThemeService,
		private dialog: MatDialog
	) {
		this.themeService.initTheme();
		this.isLightMode = this.themeService.isLightMode();
	}

	navigateToProfile(): void {
		const userId = this.currentUserService.currentUser!.id;
		this.router.navigate(['main/users', userId]);
	}

	onNavigationToggle(): void {
		this.toggleSide.emit();
	}

	changePassword(): void {
		const dialogRef = this.dialog.open(ChangePasswordFormComponent, {
			width: '60vw',
			maxWidth: '500px',
			autoFocus: false,
			disableClose: true,
		});
	}

	toggleLightMode($event: Event): void {
		$event.stopPropagation();
		this.changeIcon = !this.changeIcon;
		this.isLightMode = this.themeService.isLightMode();

		this.isLightMode
			? this.themeService.update(Theme.darkMode)
			: this.themeService.update(Theme.lightMode);
	}

	logout(): void {
		this.localStorageUtil.deleteItem(jwtTokenKey);
		this.currentUserService.clearCurrentUser();
		this.router.navigate(['public']);
	}
}
