import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormGroup,
	Validators,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs';
import { passwordRegex } from '../../../../../core/constants/regexes';
import { ValidationViolation } from '../../../../../core/models/validation-violation.model';
import { CurrentUserService } from '../../../../../core/services/current-user.service';
import { confirmValidator } from '../../../../../core/utils/validators/confirm.validator';
import { UsersService } from '../../../../users/services/users.service';

@Component({
	selector: 'app-change-password-form',
	templateUrl: './change-password-form.component.html',
	styleUrls: ['./change-password-form.component.scss'],
})
export class ChangePasswordFormComponent implements OnInit {
	changePassForm: FormGroup;
	passPatternDesc: string =
		'Must be at least 6 characters long with a number and special symbol';

	constructor(
		private fb: FormBuilder,
		private userService: UsersService,
		private dialogRef: MatDialogRef<ChangePasswordFormComponent>,
		private snackBar: MatSnackBar,
		private currentUserService: CurrentUserService
	) {
		this.changePassForm = this.fb.group({
			oldPass: ['', Validators.required],
			newPass: [
				'',
				[Validators.required, Validators.pattern(passwordRegex)],
			],
			confirmNewPass: ['', Validators.required],
		});
	}

	get oldPassControl(): AbstractControl<string> | null {
		return this.changePassForm.get('oldPass');
	}

	get newPassControl(): AbstractControl<string> | null {
		return this.changePassForm.get('newPass');
	}

	get confirmNewPassControl(): AbstractControl<string> | null {
		return this.changePassForm.get('confirmNewPass');
	}

	ngOnInit(): void {
		this.confirmNewPassControl?.addValidators(
			confirmValidator(this.newPassControl!, 'new password')
		);
	}

	onSubmit(): void {
		const oldPass = this.changePassForm.value['oldPass'];
		const newPass = this.changePassForm.value['newPass'];
		const id = this.currentUserService.currentUser!.id;

		const observer = {
			next: () => {
				this.dialogRef.close();
				this.snackBar.open(`Successfully updated password.`, 'Ok');
			},
			error: (httpError: HttpErrorResponse) => {
				this.snackBar.open(
					httpError.error.violations
						.map(
							(violation: ValidationViolation) =>
								violation.message
						)
						.join(', '),
					'Close'
				);
			},
		};
		this.userService
			.changePassword(id, oldPass, newPass)
			.pipe(take(1))
			.subscribe(observer);
	}
}
