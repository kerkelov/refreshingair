// Builtin
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Routing
import { RouterModule } from '@angular/router';

//Modules
import { SharedModule } from '../../shared/shared.module';

// Material
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';

// Components
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SideItemComponent } from './side-item/side-item.component';
import { ChangePasswordFormComponent } from './header/modals/change-password-form/change-password-form.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
	declarations: [
		HeaderComponent,
		SidebarComponent,
		SideItemComponent,
		ChangePasswordFormComponent,
	],
	imports: [
		// Builtin
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		//Routing
		RouterModule,
		// Shared
		SharedModule,
		// Material
		MatIconModule,
		MatMenuModule,
		MatSlideToggleModule,
		MatButtonModule,
		MatDialogModule,
		MatInputModule,
		MatSnackBarModule,
	],
	exports: [HeaderComponent, SidebarComponent],
})
export class LayoutModule {}
