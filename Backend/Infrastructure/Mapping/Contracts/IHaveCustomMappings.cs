﻿using AutoMapper;

namespace Infrastructure.Mapping.Contracts;

public interface IHaveCustomMappings
{
    void CreateMappings(IProfileExpression config);
}
