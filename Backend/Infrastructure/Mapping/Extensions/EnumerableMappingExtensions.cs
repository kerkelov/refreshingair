using System.Collections;
using Infrastructure.Mapping;

namespace RefreshingAir.Mapping.Extensions;

public static class EnumerableMappingExtensions
{
    public static IEnumerable<TDestination> To<TDestination>(
        this IEnumerable source)
    {
        if (source == null)
        {
            throw new ArgumentNullException(nameof(source));
        }

        foreach (var item in source)
        {
            yield return AutoMapperConfig.MapperInstance.Map<TDestination>(item);
        }
    }
}