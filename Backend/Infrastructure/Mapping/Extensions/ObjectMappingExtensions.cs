using Infrastructure.Mapping;
using RefreshingAir.Infrastructure.Extensions;

namespace RefreshingAir.Mapping.Extensions;

public static class ObjectMappingExtensions
{
    public static T To<T>(this object origin)
    {
        if (origin == null)
        {
            throw new ArgumentNullException(nameof(origin));
        }

        return AutoMapperConfig.MapperInstance.Map<T>(origin);
    }

    public static T MapAgainst<T>(this T target, object source)
    {
        source
        .GetType()
        .GetProperties()
        .ForEach(sourceProperty =>
        {
            var targetProperty = target.GetType().GetProperty(sourceProperty.Name);

            if (targetProperty != null)
            {
                object value = sourceProperty.GetValue(source);
                targetProperty.SetValue(target, value);
            }
        });

        return target;
    }
}