﻿using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data.Base;
using System.Runtime.CompilerServices;

namespace RefreshingAir.Infrastructure.Extensions;

public static class QueriableExtensions
{
    public static async Task<T> FindByIdAsync<T>(this IQueryable<T> queriable, Guid id)
        where T : BaseEntity
        => await queriable.FirstOrDefaultAsync(element => element.Id == id);
}
