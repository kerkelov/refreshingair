using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RefreshingAir.Data;
using RefreshingAir.Data.Entities;
using RefreshingAir.Infrastructure.Filters;
using RefreshingAir.Shared;
using Shared.ConventionalServices;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Infrastructure.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection SetupIdentity(this IServiceCollection services)
    {
        services
            .AddIdentity<User, Role>(options => IdentityOptionsProvider.GetIdentityOptions(options))
            .AddRoles<Role>()
            .AddEntityFrameworkStores<AppDbContext>()
            .AddDefaultTokenProviders();

        return services;
    }

    public static IServiceCollection SetupDbContext(this IServiceCollection services, IConfiguration configuration)
        => services.AddDbContext<AppDbContext>(options =>
           {
               options.UseSqlServer(configuration.GetDefaultConnectionString());
           });

    public static IServiceCollection SetupJwtAuthentication(this IServiceCollection services, AppSettings appSettings)
    {
        var key = Encoding.ASCII.GetBytes(appSettings.Secret);

        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.RequireHttpsMetadata = false;
            options.SaveToken = false;
            options.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false
            };
        });

        return services;
    }

    public static IServiceCollection SetupCors(this IServiceCollection services)
    => services
        .AddCors(options => options
            .AddPolicy(
                name: AllowLocalHostAngularCorsPolicy,
                policy => policy
                    .WithOrigins("http://localhost:4200")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
            ));

    public static IServiceCollection SetupConventionalServices(this IServiceCollection services)
    {
        Type transientServiceInterface = typeof(ITransientService),
            scopedServiceInterface = typeof(IScopedService),
            singletonServiceInterface = typeof(ISingletonService);

        var typesToRegister = GetInterfaceAndImplementationFromAssemblies(typeof(ApiController));

        typesToRegister.ForEach(type =>
        {
            var typeToRegister = new { Interface = (Type)type.Interface, Implementation = (Type)type.Implementation };

            if (transientServiceInterface.IsAssignableFrom(type.Interface))
            {
                services.AddTransient(typeToRegister.Interface, typeToRegister.Implementation);
            }
            else if (scopedServiceInterface.IsAssignableFrom(type.Interface))
            {
                services.AddScoped(typeToRegister.Interface, typeToRegister.Implementation);
            }
            else if (singletonServiceInterface.IsAssignableFrom(type.Interface))
            {
                services.AddSingleton(typeToRegister.Interface, typeToRegister.Implementation);
            }
        });

        return services;

        IEnumerable<dynamic> GetInterfaceAndImplementationFromAssemblies(params Type[] typesFromTheAssemblies)
        {
            IEnumerable<dynamic> result = new List<dynamic>();

            typesFromTheAssemblies.ForEach(type =>
            {
                var publicTypesFromTheAssembly = type
                    .Assembly
                    .GetExportedTypes()
                    .Where(t => t.IsClass && !t.IsAbstract)
                    .Select(t => new
                    {
                        Interface = t.GetInterface($"I{t.Name}"),
                        Implementation = t
                    })
                    .Where(t => t.Interface != null);

                result = result.Concat(publicTypesFromTheAssembly);
            });

            return result;
        }
    }

    public static IServiceCollection SetupControllers(this IServiceCollection services)
    {
        services.AddControllers(options => options.Filters.Add<ModelOrNotFoundActionFilter>());

        return services;
    }
    public static IServiceCollection SetupSwaggerGen(this IServiceCollection services)
        => services.AddSwaggerGen(option =>
           {
               option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
               {
                   Name = "Authorization",
                   Type = SecuritySchemeType.ApiKey,
                   Scheme = "Bearer",
                   BearerFormat = "JWT",
                   In = ParameterLocation.Header,
                   Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 1safsfsdfdfd\"",
               });

               option.AddSecurityRequirement(new OpenApiSecurityRequirement
               {
                   {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}
                   }
               });
           });
}

