using System.Reflection;
using Infrastructure.Mapping;
using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data;
using RefreshingAir.Data.Entities;
using RefreshingAir.Data.Seeding;

namespace RefreshingAir.Infrastructure.Extensions;

public static class IApplicationBuilderExtensions
{

    public static IApplicationBuilder SetupAutomapper(this IApplicationBuilder app)
    {
        AutoMapperConfig.RegisterMappings(typeof(User).GetTypeInfo().Assembly);

        return app;
    }

    public static IApplicationBuilder SetupSeeding(this IApplicationBuilder app, IServiceProvider serviceProvider)
    {
        using (var scope = serviceProvider.CreateScope())
        {
            var dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();

            //dbContext.Database.EnsureDeleted();

            dbContext.Database.Migrate();
            new AppDbContextSeeder().SeedAsync(dbContext, scope.ServiceProvider).GetAwaiter().GetResult();
        }

        return app;
    }

    public static IApplicationBuilder SetupSwaggerUI(this IApplicationBuilder app, IWebHostEnvironment environment)
    {
        if (environment.IsDevelopment())
        {
            app.UseSwagger(options =>
            {
                options.RouteTemplate = "api/swagger/{documentname}/swagger.json";
            });

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/api/swagger/v1/swagger.json", "RefreshingAir API v1");
                options.RoutePrefix = "api/swagger";
            });
        }

        return app;
    }
}
