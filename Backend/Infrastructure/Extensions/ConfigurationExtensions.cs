namespace RefreshingAir.Infrastructure.Extensions;

public static class ConfigurationExtensions
{
    public static string GetDefaultConnectionString(this IConfiguration config) =>
        config?.GetConnectionString("DefaultConnection");

    public static AppSettings GetAppSettings(this IServiceCollection services, IConfiguration config)
    {
        var appSettingsConfiguration = config.GetSection("ApplicationSettings");
        var appSettings = appSettingsConfiguration.Get<AppSettings>();
        services.Configure<AppSettings>(appSettingsConfiguration);

        return appSettings;
    }
}