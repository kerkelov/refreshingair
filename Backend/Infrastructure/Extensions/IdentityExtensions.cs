using Microsoft.AspNetCore.Identity;

namespace RefreshingAir.Infrastructure.Extensions;

public static class IdentityExtensions
{
    public static string ErrorMessage(this IdentityResult result)
        => result.Errors.First().Description;
}