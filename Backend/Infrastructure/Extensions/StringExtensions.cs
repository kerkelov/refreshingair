﻿using System.Runtime.CompilerServices;

namespace RefreshingAir.Infrastructure.Extensions;

public static class StringExtensions
{
    public static Guid ToGuid(this string guid)
        => Guid.Parse(guid);
}
