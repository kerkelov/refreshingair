﻿using System.Security.Claims;
using RefreshingAir.Infrastructure.Extensions;
using RefreshingAir.Infrastructure.Services.Contracts;
using Shared.ConventionalServices;

namespace RefreshingAir.Infrastructure.Services;

public class CurrentUserService : ICurrentUserService, IScopedService
{
    private readonly ClaimsPrincipal user;

    public CurrentUserService(IHttpContextAccessor httpContextAccessor) => user = httpContextAccessor?.HttpContext?.User;

    public IEnumerable<string> Roles => user
        .Claims
        .Where(claim => claim.Type == ClaimTypes.Role)
        .Select(role => role.Value);

    public string Email => user
        .Claims
        .FirstOrDefault(claim => claim.Type == ClaimTypes.Email)
        .Value;

    public bool IsThereAUser => this.user is not null && this.user.Claims.Any();

    public Guid Id => this.user
        .Claims
        .FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)
        .Value
        .ToGuid();
}

