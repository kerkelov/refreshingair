﻿using Shared.ConventionalServices;

namespace RefreshingAir.Infrastructure.Services.Contracts;

public interface ICurrentUserService : IScopedService
{
    IEnumerable<string> Roles { get; }

    Guid Id { get; }

    string Email { get; }

    bool IsThereAUser { get; }
}
