﻿using Microsoft.AspNetCore.Identity;
using RefreshingAir.Data.Entities;
using Shared.ConventionalServices;

namespace RefreshingAir.Infrastructure.Services.Contracts;

internal interface IRoleManager : ITransientService
{
    Task<IdentityResult> CreateAsync(Role role);

    Task<Role> FindByNameAsync(string name);
}