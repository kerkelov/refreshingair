﻿using Microsoft.AspNetCore.Identity;
using RefreshingAir.Data.Entities;
using Shared.ConventionalServices;

namespace RefreshingAir.Infrastructure.Services.Contracts;

public interface IUserManager : ITransientService
{
    Task<User> FindByEmailAsync(string email);

    Task<User> FindByIdAsync(string Id);

    Task<IdentityResult> AddToRoleAsync(User user, string roleName);

    Task<IdentityResult> CreateAsync(User user, string password);

    Task<IdentityResult> DeleteAsync(User user);

    Task<IdentityResult> UpdateAsync(User user);

    Task<IList<string>> GetRolesAsync(User user);

    Task<bool> CheckPasswordAsync(User user, string password);

    Task<string> HashPasswordAsync(User user, string password);


}
