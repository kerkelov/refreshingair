﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using RefreshingAir.Data.Entities;
using RefreshingAir.Infrastructure.Services.Contracts;

namespace RefreshingAir.Infrastructure.Services;

public class UserManager : UserManager<User>, IUserManager
{
    public UserManager(IUserStore<User> store,
        IOptions<IdentityOptions> optionsAccessor,
        IPasswordHasher<User> passwordHasher,
        IEnumerable<IUserValidator<User>> userValidators,
        IEnumerable<IPasswordValidator<User>> passwordValidators,
        ILookupNormalizer keyNormalizer,
        IdentityErrorDescriber errors,
        IServiceProvider services,
        ILogger<UserManager<User>> logger)
        : base(store,
            optionsAccessor,
            passwordHasher,
            userValidators,
            passwordValidators,
            keyNormalizer,
            errors,
            services,
            logger)
    { }

    public Task<string> HashPasswordAsync(User user, string password)
        => Task.FromResult(base.PasswordHasher.HashPassword(user, password));
}
