﻿using Microsoft.AspNetCore.Identity;
using RefreshingAir.Data.Entities;
using RefreshingAir.Infrastructure.Services.Contracts;

namespace RefreshingAir.Infrastructure.Services;

public class RoleManager : RoleManager<Role>, IRoleManager
{
    public RoleManager(IRoleStore<Role> store,
        IEnumerable<IRoleValidator<Role>> roleValidators,
        ILookupNormalizer keyNormalizer,
        IdentityErrorDescriber errors,
        ILogger<RoleManager<Role>> logger)
        : base(store,
            roleValidators,
            keyNormalizer,
            errors,
            logger)
    {
    }
}
