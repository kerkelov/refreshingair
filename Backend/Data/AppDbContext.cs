﻿using System.Reflection;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using RefreshingAir.Data.Base.Contracts;
using RefreshingAir.Data.Entities;
using RefreshingAir.Infrastructure.Extensions;
using RefreshingAir.Infrastructure.Services.Contracts;

namespace RefreshingAir.Data;

public class AppDbContext : IdentityDbContext<User, Role, Guid>
{
    private readonly ICurrentUserService currentUser;

    public AppDbContext(DbContextOptions<AppDbContext> options, ICurrentUserService currentUserService)
        : base(options) => this.currentUser = currentUserService;

    public DbSet<AirConditioner> AirConditioners { get; set; }

    public DbSet<Order> Orders { get; set; }

    public DbSet<Part> Parts { get; set; }
    public DbSet<AirConOrder> AirConOrders { get; set; }

    public DbSet<Technician> Technicians { get; set; }

    public override int SaveChanges(bool acceptAllChangesOnSuccess)
    {
        this.ApplyAuditInfoRules();
        return base.SaveChanges(acceptAllChangesOnSuccess);
    }

    public override Task<int> SaveChangesAsync(
        bool acceptAllChangesOnSuccess,
        CancellationToken cancellationToken = default)
    {
        this.ApplyAuditInfoRules();
        return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        var entityTypes = builder.Model.GetEntityTypes().ToList();
        base.OnModelCreating(builder);

        SetDeleteBehaviorToRestrict(entityTypes);
        builder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
        SetupDeletedQueryFilter(builder, entityTypes);
    }

    private static void SetDeleteBehaviorToRestrict(IList<IMutableEntityType> entityTypes)
        => entityTypes
            .SelectMany(entity =>
                entity
                .GetForeignKeys()
                .Where(foreignKey => foreignKey.DeleteBehavior == DeleteBehavior.Cascade))
            .ToList()
            .ForEach(foreignKey => foreignKey.DeleteBehavior = DeleteBehavior.Restrict);

    private static void SetIsDeletedQueryFilter<T>(ModelBuilder builder) where T : class, IDeletableEntity<Guid> => builder.Entity<T>().HasQueryFilter(entity => !entity.IsDeleted);
    private static void SetupDeletedQueryFilter(ModelBuilder builder, IList<IMutableEntityType> entityTypes)
    {
        var SetIsDeletedQueryFilterMethodInfo = typeof(AppDbContext)
            .GetMethod(nameof(SetIsDeletedQueryFilter), BindingFlags.NonPublic | BindingFlags.Static);

        var deletableEntityTypes = entityTypes
            .Where(entity => entity.ClrType != null && typeof(IDeletableEntity<Guid>).IsAssignableFrom(entity.ClrType));

        deletableEntityTypes.ForEach(deletableEntityType =>
            SetIsDeletedQueryFilterMethodInfo
            .MakeGenericMethod(deletableEntityType.ClrType)
            .Invoke(null, new object[] { builder }));
    }

    private void ApplyAuditInfoRules()
        => this.ChangeTracker
            .Entries()
            .ToList()
            .ForEach(entry =>
            {
                if (entry.Entity is IDeletableEntity<Guid> deletableEntity && entry.State == EntityState.Deleted)
                {
                    if (this.currentUser.IsThereAUser)
                    {
                        deletableEntity.DeletedBy = this.currentUser.Id;
                    }

                    deletableEntity.DeletedOn = DateTime.UtcNow;
                    deletableEntity.IsDeleted = true;

                    entry.State = EntityState.Modified;

                    //this return is to stop the method from assigning modifiedOn and modifiedBy properties
                    //when an entity is deleted
                    return;
                }
                else if (entry.Entity is IAuditInfoEntity<Guid> entity)
                {
                    if (entry.State == EntityState.Added)
                    {
                        entity.CreatedOn = DateTime.UtcNow;

                        if (this.currentUser.IsThereAUser)
                        {
                            entity.CreatedBy = this.currentUser.Id;
                        }
                    }
                    else if (entry.State == EntityState.Modified)
                    {
                        entity.ModifiedOn = DateTime.UtcNow;

                        if (this.currentUser.IsThereAUser)
                        {
                            entity.ModifiedBy = this.currentUser.Id;
                        }
                    }
                }
            });
}
