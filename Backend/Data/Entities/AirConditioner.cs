﻿using System.ComponentModel.DataAnnotations;
using RefreshingAir.Data.Base;

namespace RefreshingAir.Data.Entities;

public class AirConditioner : DeletableEntity
{
    [Required]
    public string Model { get; set; }

    [Required]
    public string Country { get; set; }

    [Required]
    public string Manufacturer { get; set; }

    [Required]
    public string PictureUrl { get; set; }

    [Required]
    public double Price { get; set; }

    public string Description { get; set; }

    [Required]
    public AirConditionerType Type { get; set; }
}
