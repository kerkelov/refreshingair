﻿using RefreshingAir.Data.Base;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Data.Entities;

public class Part : DeletableEntity
{
    public virtual Order Order { get; set; }

    [Required]
    public Guid OrderId { get; set; }

    [Required]
    public int Quantity { get; set; }

    [Required]
    public string Name { get; set; }
}
