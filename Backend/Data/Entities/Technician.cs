﻿using RefreshingAir.Data.Base;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Data.Entities;

public class Technician : DeletableEntity
{
    [Required]
    public string Location { get; set; }

    public User User { get; set; }

    public Guid UserId { get; set; }

    public virtual IList<Order> Orders { get; set; }
}
