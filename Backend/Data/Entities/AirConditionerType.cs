﻿namespace RefreshingAir.Data.Entities;

public enum AirConditionerType
{
    Portable = 0,

    Window = 1,

    Central = 2,
}
