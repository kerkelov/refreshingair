﻿using Microsoft.AspNetCore.Identity;
using RefreshingAir.Data.Base.Contracts;

namespace RefreshingAir.Data.Entities;

public class Role : IdentityRole<Guid>, IAuditInfoEntity<Guid>, IDeletableEntity<Guid>
{
    public Role(string name) : base(name)
        => this.Id = Guid.NewGuid();

    public DateTime CreatedOn { get; set; }

    public Guid? CreatedBy { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public Guid? ModifiedBy { get; set; }

    public bool IsDeleted { get; set; }

    public DateTime? DeletedOn { get; set; }

    public Guid? DeletedBy { get; set; }
}
