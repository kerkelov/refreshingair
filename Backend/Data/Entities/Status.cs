﻿namespace RefreshingAir.Data.Entities;

// TODO: is it ok if this status is used in the service layer as well because currently it is used.
public enum Status
{
    Pending = 0,
    InProgress = 1,
    Done = 2,
}
