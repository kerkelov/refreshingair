﻿using RefreshingAir.Data.Base;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Data.Entities;

public class Order : DeletableEntity
{
    public AirConditioner AirConditioner { get; set; }

    [Required]
    public Guid AirConditionerId { get; set; }

    public User Client { get; set; }

    public Guid ClientId { get; set; }

    public Technician Technician { get; set; }

    public Guid TechnicianId { get; set; }

    public virtual IList<Part> PartsNeeded { get; set; }

    [Required]
    public Status Status { get; set; }

    [Required]
    public DateTime Deadline { get; set; }

    public DateTime? Started { get; set; }

    public DateTime? Finished { get; set; }

}
