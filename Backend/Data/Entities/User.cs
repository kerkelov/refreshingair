﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using RefreshingAir.Data.Base.Contracts;

namespace RefreshingAir.Data.Entities;
public class User : IdentityUser<Guid>, IAuditInfoEntity<Guid>, IDeletableEntity<Guid>
{
    public User()
    {
        this.Roles = new HashSet<IdentityUserRole<Guid>>();
        this.Claims = new HashSet<IdentityUserClaim<Guid>>();
        this.Logins = new HashSet<IdentityUserLogin<Guid>>();
        this.ProfilePictureUrl = "https://res.cloudinary.com/donhvedgr/image/upload/v1663345678/mkunrvxxbwrdovxkwtdm.webp";
        this.Id = Guid.NewGuid();
    }

    [Required]
    public string FirstName { get; set; }

    [Required]
    public string LastName { get; set; }

    public string ProfilePictureUrl { get; set; }

    [Required]
    public DateTime CreatedOn { get; set; }

    public Guid? CreatedBy { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public Guid? ModifiedBy { get; set; }

    [Required]
    public bool IsDeleted { get; set; }

    public DateTime? DeletedOn { get; set; }

    public Guid? DeletedBy { get; set; }

    public virtual ICollection<IdentityUserRole<Guid>> Roles { get; set; }

    public virtual ICollection<IdentityUserClaim<Guid>> Claims { get; set; }

    public virtual ICollection<IdentityUserLogin<Guid>> Logins { get; set; }
}
