﻿using RefreshingAir.Data.Base;

namespace RefreshingAir.Data.Entities;

public class AirConOrder : DeletableEntity
{
    public AirConditioner AirConditioner { get; set; }

    public Guid AirConditionerId { get; set; }

    public User Client { get; set; }

    public Guid ClientId { get; set; }
}