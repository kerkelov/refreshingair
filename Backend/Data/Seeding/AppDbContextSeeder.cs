﻿using RefreshingAir.Data.Seeding.Seeders;
using RefreshingAir.Infrastructure.Extensions;

namespace RefreshingAir.Data.Seeding;

public class AppDbContextSeeder : ISeeder
{
    public async Task SeedAsync(AppDbContext dbContext, IServiceProvider serviceProvider)
    {
        return;
        if (serviceProvider == null)
        {
            throw new ArgumentNullException(nameof(serviceProvider));
        }

        var logger = serviceProvider.GetService<ILoggerFactory>().CreateLogger(typeof(AppDbContextSeeder));

        var seeders = new List<ISeeder>()
        {
            new RolesSeeder(),
            new UsersSeeder(),
            new AirConditionersSeeder(),
            new OrdersSeeder(),
            new PartsSeeder(),
        };

        await seeders.ForEachAsync(async seeder =>
        {
            await seeder.SeedAsync(dbContext, serviceProvider);
            await dbContext.SaveChangesAsync();

            logger.LogInformation($"Seeder {seeder.GetType().Name} done.");
        });
    }
}
