﻿using RefreshingAir.Data.Entities;

namespace RefreshingAir.Data.Seeding.Seeders;

public class AirConditionersSeeder : ISeeder
{
    private IEnumerable<AirConditioner> airConditioners = new[]
    {
        new AirConditioner
        {
            Model = "Hitachi Shirocuma",
            PictureUrl = "https://res.cloudinary.com/donhvedgr/image/upload/v1681813221/RefreshingAir/4089_37286_HITACHI.RAF-25FXE_RAC-25FXE_SHIROKUMA6_0x250_jomges.jpg",
            Price = 1700,
            Country = "Japan",
            Manufacturer = "Daikin Corp",
            Description = "The window air conditioner is a classic choice for anyone looking to cool a larger room or multiple rooms in a home or office. It's designed to fit easily into most standard windows and can be installed quickly and easily. With multiple fan speeds and cooling options, this air conditioner can keep you comfortable no matter how hot it gets outside.",
            Type = AirConditionerType.Window
        },
        new AirConditioner
        {
            Model = "Gree Fairy ||",
            PictureUrl = "https://res.cloudinary.com/donhvedgr/image/upload/v1681813070/RefreshingAir/12FITH1C_0x250_bqqdcx.png",
            Price = 800,
            Country = "USA",
            Manufacturer = "Carrier Corp",
            Description = "The central air conditioner is a powerful system designed to cool an entire home or building. It's typically installed by a professional and requires ductwork to distribute cool air throughout the space. With energy-efficient options and advanced temperature control features, a central air conditioner is a great choice for anyone looking to stay cool and comfortable all summer long.",
            Type = AirConditionerType.Central
        },
        new AirConditioner
        {
            Model = "Fuji Electric",
            PictureUrl  = "https://res.cloudinary.com/donhvedgr/image/upload/v1681813183/RefreshingAir/445_0x250_hu901y.jpg",
            Price = 1300,
            Country = "USA",
            Manufacturer = "Carrier Corp",
            Description = "The portable air conditioner is perfect for cooling individual rooms or small spaces. It's easy to move from room to room, and doesn't require any complicated installation or special tools. With adjustable settings and a built-in air purifier, this air conditioner is an excellent choice for anyone who wants to stay cool and comfortable during the hot summer months",
            Type = AirConditionerType.Portable
        }
    };

    public async Task SeedAsync(AppDbContext dbContext, IServiceProvider serviceProvider)
        => await dbContext.AddRangeAsync(airConditioners);
}
