﻿using Microsoft.AspNetCore.Identity;
using RefreshingAir.Data.Entities;
using RefreshingAir.Infrastructure.Extensions;
using RefreshingAir.Infrastructure.Services.Contracts;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Data.Seeding.Seeders;

public class RolesSeeder : ISeeder
{
    public async Task SeedAsync(AppDbContext dbContext, IServiceProvider serviceProvider)
    {
        var roleManager = serviceProvider.GetRequiredService<IRoleManager>();

        await SeedRoleAsync(roleManager, AdministratorRoleName);
        await SeedRoleAsync(roleManager, ClientRoleName);
        await SeedRoleAsync(roleManager, TechnicianRoleName);
    }

    private static async Task SeedRoleAsync(IRoleManager roleManager, string roleName)
    {
        var role = await roleManager.FindByNameAsync(roleName);

        if (role == null)
        {
            var result = await roleManager.CreateAsync(new Role(roleName));

            if (!result.Succeeded)
            {
                throw new Exception(result.ErrorMessage());
            }
        }
    }
}
