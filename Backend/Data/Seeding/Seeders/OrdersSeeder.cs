﻿using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data.Entities;
using RefreshingAir.Infrastructure.Extensions;
using static RefreshingAir.Data.Seeding.SeedingConstants;

namespace RefreshingAir.Data.Seeding.Seeders;

public class OrdersSeeder : ISeeder
{
    private IEnumerable<Order> orders = new[]
    {
        new Order
        {
            Status = Status.Pending,
            Deadline = DateTime.UtcNow.AddDays(30),
        },
        new Order
        {
            Status = Status.Done,
            Deadline = DateTime.UtcNow.AddDays(20),
            Started = DateTime.UtcNow.Subtract(TimeSpan.FromDays(10)),
            Finished = DateTime.UtcNow
        },
        new Order
        {
            Status = Status.InProgress,
            Deadline = DateTime.UtcNow.AddDays(150),
            Started = DateTime.UtcNow
        },
    };

    public async Task SeedAsync(AppDbContext dbContext, IServiceProvider serviceProvider)
    {
        var technician = await dbContext
            .Technicians
            .Include(technician => technician.User)
            .FirstAsync(technician => technician.User.FirstName == TechnicianFirstName);

        var client = await dbContext
            .Users
            .FirstAsync(user => user.FirstName == ClientFirstName);

        var airconditioners = await dbContext
            .AirConditioners
            .ToListAsync();

        orders.ForEach(order =>
        {
            order.ClientId = client.Id;
            order.TechnicianId = technician.Id;
            order.AirConditionerId = airconditioners[Random.Shared.Next(airconditioners.Count)].Id;

            dbContext.Add(order);
        });
    }
}