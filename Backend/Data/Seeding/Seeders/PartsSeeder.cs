﻿using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data.Entities;

namespace RefreshingAir.Data.Seeding.Seeders;

public class PartsSeeder : ISeeder
{
    private Part[] parts =
    {
        new Part
        {
            Name = "Exhaust hose",
            Quantity = 1,
        },
        new Part
        {
            Name = "Exhaust hose adapter",
            Quantity = 1,
        }
    };

    public async Task SeedAsync(AppDbContext dbContext, IServiceProvider serviceProvider)
    {
        var orders = await dbContext
            .Orders
            .Where(order => order.Status != Status.Pending)
            .ToListAsync();

        for (int i = 0; i < parts.Length; i++)
        {
            parts[i].Order = orders[i];

            dbContext.Add(parts[i]);
        }
    }
}