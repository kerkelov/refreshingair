﻿namespace RefreshingAir.Data.Seeding;

public class SeedingConstants
{
    public const string AdministratorFirstName = "Gosho";
    public const string TechnicianFirstName = "Pesho";
    public const string ClientFirstName = "Tosho";

    public const string InitialAdministratorPassword = "Gosho1!";
    public const string InitialTechnicianPassword = "Pesho1!";
    public const string InitialClientPassword = "Tosho1!";

    public const string InitialAdministratorEmail = "stefanov@abv.bg";
    public const string InitialTechnicianEmail = "petrov@abv.bg";
    public const string InitialClientEmail = "ivanov@abv.bg";
}
