﻿using RefreshingAir.Data.Base.Contracts;

namespace RefreshingAir.Data.Base;

public abstract class DeletableEntity : AuditInfoEntity, IDeletableEntity<Guid>
{
    public bool IsDeleted { get; set; }

    public DateTime? DeletedOn { get; set; }

    public Guid? DeletedBy { get; set; }
}
