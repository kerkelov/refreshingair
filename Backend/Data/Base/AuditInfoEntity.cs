﻿using System.ComponentModel.DataAnnotations;
using RefreshingAir.Data.Base;
using RefreshingAir.Data.Base.Contracts;

namespace RefreshingAir.Data.Base;

public abstract class AuditInfoEntity : BaseEntity, IAuditInfoEntity<Guid>
{
    [Required]
    public DateTime CreatedOn { get; set; }

    public Guid? CreatedBy { get; set; }

    public DateTime? ModifiedOn { get; set; }

    public Guid? ModifiedBy { get; set; }
}
