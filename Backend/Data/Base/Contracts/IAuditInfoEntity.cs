﻿namespace RefreshingAir.Data.Base.Contracts;

public interface IAuditInfoEntity<TKey> : IBaseEntity<Guid>
    where TKey : struct
{
    DateTime CreatedOn { get; set; }

    TKey? CreatedBy { get; set; }

    TKey? ModifiedBy { get; set; }

    DateTime? ModifiedOn { get; set; }
}
