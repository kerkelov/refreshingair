﻿namespace RefreshingAir.Data.Base.Contracts;

public interface IBaseEntity<TKey>
{
    TKey Id { get; set; }
}