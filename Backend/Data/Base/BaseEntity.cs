﻿using RefreshingAir.Data.Base.Contracts;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Data.Base;

public abstract class BaseEntity : IBaseEntity<Guid>
{
    public BaseEntity() => this.Id = Guid.NewGuid();

    [Key]
    public Guid Id { get; set; }
}
