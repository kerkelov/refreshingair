﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RefreshingAir.Data.Entities;

namespace RefreshingAir.Data.Configurations;

public class UserConfiguration : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> user)
    {
        user
            .HasMany(user => user.Claims)
            .WithOne()
            .HasForeignKey(claim => claim.UserId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Restrict);

        user
            .HasMany(user => user.Logins)
            .WithOne()
            .HasForeignKey(login => login.UserId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Restrict);

        user
            .HasMany(user => user.Roles)
            .WithOne()
            .HasForeignKey(role => role.UserId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Restrict);
    }
}
