﻿using System.Runtime.CompilerServices;

namespace RefreshingAir.Shared.ResultObject;

public class OperationResult
{
    public bool Succeeded { get; set; }

    public bool Failure => !Succeeded;

    public string ErrorMessage { get; set; }

    public static implicit operator OperationResult(bool succeeded)
        => new OperationResult { Succeeded = succeeded };

    public static implicit operator OperationResult(string errorMessage)
        => new OperationResult { Succeeded = false, ErrorMessage = errorMessage };
}
