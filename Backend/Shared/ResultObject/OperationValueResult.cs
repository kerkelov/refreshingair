﻿namespace RefreshingAir.Shared.ResultObject;

public class OperationValueResult<TValue> : OperationResult
{
    public TValue Value { get; private set; }

    public OperationValueResult() { }

    public OperationValueResult(string errorMessage)
    {
        this.ErrorMessage = errorMessage;
        this.Succeeded = false;
    }

    public OperationValueResult(TValue value)
    {
        this.Value = value;
        this.Succeeded = true;
    }

    public static implicit operator OperationValueResult<TValue>(TValue value)
        => new() { Value = value, Succeeded = true };
}
