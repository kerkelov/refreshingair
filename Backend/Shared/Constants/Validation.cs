namespace RefreshingAir.Shared.Constants;

public class Validation
{
    public class Technician
    {
        public const int MaxNameLength = 40;
    }

    public class AirConditioner
    {
        public const int MaxInventorLength = 25;

        public const int MaxCountryLength = 20;

        public const int MaxModelLength = 30;
    }
}