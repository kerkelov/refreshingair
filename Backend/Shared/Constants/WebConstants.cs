﻿namespace RefreshingAir.Shared.Constants;

public static class WebConstants
{
    public const string SystemName = "RefreshingAir";
    public static string AllowLocalHostAngularCorsPolicy = "angular cors policymm";

    public const string AdministratorRoleName = "Administrator";
    public const string TechnicianRoleName = "Technician";
    public const string ClientRoleName = "Client";

    public const string RouteId = "{id}";

    public const string IdNotFoundError = "This id does not exist";
    public const string EmailNotFoundError = "This email does not exist";
    public const string IncorrectPasswordError = "This password is incorrect";

    public const string SecondaryAppSecret = "magicunicorns17";
}
