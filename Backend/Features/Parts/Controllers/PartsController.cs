using Microsoft.AspNetCore.Mvc;
using RefreshingAir.Features.Parts.Models.Requests;
using RefreshingAir.Features.Parts.Models.Responses;
using RefreshingAir.Features.Parts.Models.ServiceModels;
using RefreshingAir.Features.Parts.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Parts.Controllers;

public class PartsController : ApiController
{
    private readonly IPartsService partsService;

    public PartsController(IPartsService partsService)
        => this.partsService = partsService;

    [HttpPost]
    public async Task<IActionResult> Create(CreatePartRequest request)
    {
        var model = request.To<CreatePartModel>();
        var result = await this.partsService.Create(model);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok(result.Value);
    }

    [HttpGet]
    [Route(RouteId)]
    public async Task<ActionResult<PartDetailsResponse>> GetById(Guid id)
    {
        var result = await this.partsService.GetById(id);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        var response = result.Value.To<PartDetailsModel>();

        return Ok(response);
    }

    [HttpGet]
    public async Task<IEnumerable<PartListingResponse>> GetAll()
        => (await this.partsService.GetAll())
           .Value
           .To<PartListingResponse>();

    [HttpPut]
    public async Task<IActionResult> Update(UpdatePartRequest request)
    {
        var model = request.To<UpdatePartModel>();
        var result = await this.partsService.Update(model);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }

    [HttpDelete]
    public async Task<IActionResult> Delete(Guid id)
    {
        var result = await this.partsService.Delete(id);

        if (result.Failure)
        {
            return NotFound(result.ErrorMessage);
        }

        return Ok();
    }
}