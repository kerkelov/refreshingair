﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Parts.Models.ServiceModels;

namespace RefreshingAir.Features.Parts.Models.Responses;

public class PartDetailsResponse : IMapFrom<PartDetailsModel>
{
    public Guid Id { get; set; }

    public Guid OrderId { get; set; }

    public int Quantity { get; set; }

    public string Name { get; set; }
}