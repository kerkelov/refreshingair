﻿
namespace RefreshingAir.Features.Parts.Models.ServiceModels;

public class PartListingModel
{
    public Guid OrderId { get; set; }

    public int Quantity { get; set; }

    public string Name { get; set; }
}