﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;

namespace RefreshingAir.Features.Parts.Models.ServiceModels;

public class CreatePartModel : IMapTo<Part>
{
    public Guid OrderId { get; set; }

    public int Quantity { get; set; }

    public string Name { get; set; }
}