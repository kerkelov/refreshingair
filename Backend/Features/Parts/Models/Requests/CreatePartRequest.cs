﻿using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Parts.Models.ServiceModels;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Features.Parts.Models.Requests;

public class CreatePartRequest : IMapTo<CreatePartModel>
{
    [Required]
    public Guid OrderId { get; set; }

    [Required]
    public int Quantity { get; set; }

    [Required]
    public string Name { get; set; }
}