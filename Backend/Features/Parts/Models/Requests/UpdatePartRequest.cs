﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Parts.Models.ServiceModels;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Features.Parts.Models.Requests;

public class UpdatePartRequest : IMapTo<UpdatePartModel>
{
    [Required]
    public Guid Id { get; set; }

    [Required]
    public string Model { get; set; }

    [Required]
    public string Country { get; set; }

    [Required]
    public string Inventor { get; set; }
}