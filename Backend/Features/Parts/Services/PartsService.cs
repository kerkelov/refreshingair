﻿using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.Parts.Models.ServiceModels;
using RefreshingAir.Features.Parts.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared.ResultObject;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Parts.Services;

public class PartsService : IPartsService
{
    private readonly AppDbContext data;

    public PartsService(AppDbContext dbContext)
        => this.data = dbContext;

    public async Task<OperationValueResult<Guid>> Create(CreatePartModel model)
    {
        var part = model.To<Part>();

        this.data.Add(part);
        await this.data.SaveChangesAsync();

        return part.Id;
    }

    public async Task<OperationValueResult<PartDetailsModel>> GetById(Guid id)
    {
        var part = await GetPartById(id);

        if (part is null)
        {
            return new(IdNotFoundError);
        }

        var partDetailsModel = part.To<PartDetailsModel>();

        return partDetailsModel;
    }

    public async Task<OperationValueResult<IEnumerable<PartListingModel>>> GetAll()
        => new((await this.data
                .Parts
                .ToListAsync())
                .To<PartListingModel>());

    public async Task<OperationResult> Update(UpdatePartModel model)
    {
        var part = await GetPartById(model.Id);

        if (part is null)
        {
            return IdNotFoundError;
        }

        part.MapAgainst(model);
        await this.data.SaveChangesAsync();

        return true;
    }

    public async Task<OperationResult> Delete(Guid id)
    {
        var part = await GetPartById(id);

        if (part is null)
        {
            return IdNotFoundError;
        }

        this.data.Remove(part);
        await this.data.SaveChangesAsync();

        return true;
    }

    private async Task<Part> GetPartById(Guid id)
        => await this.data
            .Parts
            .FirstOrDefaultAsync(part => part.Id == id);
}
