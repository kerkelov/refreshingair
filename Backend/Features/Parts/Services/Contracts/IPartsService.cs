﻿using RefreshingAir.Features.Parts.Models.ServiceModels;
using RefreshingAir.Shared.ResultObject;
using Shared.ConventionalServices;

namespace RefreshingAir.Features.Parts.Services.Contracts;

public interface IPartsService : ITransientService
{
    Task<OperationValueResult<Guid>> Create(CreatePartModel model);

    Task<OperationValueResult<PartDetailsModel>> GetById(Guid id);

    Task<OperationValueResult<IEnumerable<PartListingModel>>> GetAll();

    Task<OperationResult> Update(UpdatePartModel model);

    Task<OperationResult> Delete(Guid id);
}