using Infrastructure.Mapping.Contracts;

namespace RefreshingAir.Features.Users.Models.ServiceModels;

public class UserListingResponse : IMapFrom<UserListingModel>
{
    public Guid Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string UserName { get; set; }

    public string Email { get; set; }

    public string Role { get; set; }

    public string PhoneNumber { get; set; }

    public string ProfilePictureUrl { get; set; }
}