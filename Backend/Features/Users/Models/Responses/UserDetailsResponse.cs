﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Users.Models.ServiceModels;

namespace RefreshingAir.Features.Users.Models.Responses;

public class UserDetailsResponse : IMapFrom<UserDetailsModel>
{
    public Guid Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string UserName { get; set; }

    public string Email { get; set; }

    public string PhoneNumber { get; set; }

    public string ProfilePictureUrl { get; set; }

    public string CreatedOn { get; set; }

    public string Role { get; set; }
}