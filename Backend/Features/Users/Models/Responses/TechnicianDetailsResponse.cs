﻿using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Users.Models.ServiceModels;

namespace RefreshingAir.Features.Users.Models.Responses;

public class TechnicianDetailsResponse : IMapFrom<TechnicianDetailsModel>
{
    public Guid Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string UserName { get; set; }

    public string Email { get; set; }

    public string PhoneNumber { get; set; }

    public string ProfilePictureUrl { get; set; }

    public string Location { get; set; }

    public string Role { get; set; }
}