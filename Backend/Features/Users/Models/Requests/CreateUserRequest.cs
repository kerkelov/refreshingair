﻿using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Users.Models.ServiceModels;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Features.Users.Models.Requests;

public class CreateUserRequest : IMapTo<CreateUserModel>
{
    [Required]
    public string FirstName { get; set; }

    [Required]
    public string LastName { get; set; }

    [Required]
    public string UserName { get; set; }

    [Required]
    public string Email { get; set; }

    [Required]
    public string Password { get; set; }

    [Required]
    public string PhoneNumber { get; set; }

    [Required]
    public string ProfilePictureUrl { get; set; }
}