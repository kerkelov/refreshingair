﻿using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Users.Models.ServiceModels;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Features.Users.Models.Requests;

public class ChangePasswordRequest : IMapTo<ChangePasswordModel>
{
    [Required]
    public Guid Id { get; set; }

    [Required]
    public string OldPassword { get; set; }

    [Required]
    public string NewPassword { get; set; }
}