﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Users.Models.ServiceModels;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Features.Users.Models.Requests;

public class UpdateUserRequest : IMapTo<UpdateUserModel>
{
    [Required]
    public Guid Id { get; set; }

    [Required]
    public string FirstName { get; set; }

    [Required]
    public string LastName { get; set; }

    [Required]
    public string UserName { get; set; }

    [Required]
    public string Email { get; set; }

    [Required]
    public string PhoneNumber { get; set; }

    [Required]
    public string ProfilePictureUrl { get; set; }
}