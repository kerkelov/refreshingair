﻿using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Features.Users.Models;

public class CreateTechnicianRequest
{
    [Required]
    public string FirstName { get; set; }

    [Required]
    public string LastName { get; set; }

    [Required]
    public string UserName { get; set; }

    [Required]
    public string Email { get; set; }

    [Required]
    public string Password { get; set; }

    [Required]
    public string PhoneNumber { get; set; }

    [Required]
    public string ProfilePictureUrl { get; set; }

    [Required]
    public string Location { get; set; }

}