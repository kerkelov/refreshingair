﻿namespace RefreshingAir.Features.Users.Models.ServiceModels;

public class ChangePasswordModel
{
    public Guid Id { get; set; }

    public string OldPassword { get; set; }

    public string NewPassword { get; set; }
}
