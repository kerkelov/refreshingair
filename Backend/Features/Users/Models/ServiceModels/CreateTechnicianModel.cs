﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;

namespace RefreshingAir.Features.Users.Models.ServiceModels;

public class CreateTechnicianModel : IMapFrom<CreateTechnicianRequest>, IMapTo<User>
{
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string UserName { get; set; }

    public string Email { get; set; }

    public string Password { get; set; }

    public string PhoneNumber { get; set; }

    public string ProfilePictureUrl { get; set; }

    public string Location { get; set; }
}