﻿using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.Users.Models.ServiceModels;
using RefreshingAir.Features.Users.Services.Contracts;
using RefreshingAir.Infrastructure.Extensions;
using RefreshingAir.Infrastructure.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared.ResultObject;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Users.Services;

public class UsersService : IUsersService
{
    private readonly AppDbContext data;
    private readonly IUserManager userManager;

    public UsersService(AppDbContext dbContext, IUserManager userManager)
    {
        this.data = dbContext;
        this.userManager = userManager;
    }

    public async Task<OperationValueResult<Guid>> Create(CreateUserModel model)
    {
        var result = await this.userManager.CreateAsync(model.To<User>(), model.Password);

        if (result.Succeeded)
        {
            var createdUser = await this.userManager.FindByEmailAsync(model.Email);
            await this.userManager.AddToRoleAsync(createdUser, model.Role);

            return createdUser.Id;
        }

        return new(result.ErrorMessage());
    }

    public async Task<OperationValueResult<UserDetailsModel>> GetById(Guid id)
    {
        var user = await this.userManager.FindByIdAsync(id.ToString());

        if (user is null)
        {
            return new(IdNotFoundError);
        }

        var model = user.To<UserDetailsModel>();
        model.Role = (await this.userManager.GetRolesAsync(user))[0];

        return model;
    }

    public async Task<OperationValueResult<IEnumerable<UserListingModel>>> GetAll()
        => new((await this.data.Users.ToListAsync()).To<UserListingModel>());


    public async Task<OperationResult> Update(UpdateUserModel model)
    {
        var user = await this.userManager.FindByIdAsync(model.Id.ToString());

        if (user is null)
        {
            return IdNotFoundError;
        }

        var result = await this.userManager.UpdateAsync(user.MapAgainst(model));

        if (result.Succeeded)
        {
            await this.data.SaveChangesAsync();
            return true;
        }

        return result.ErrorMessage();

    }

    public async Task<OperationResult> ChangePassword(ChangePasswordModel model)
    {
        var user = await this.userManager.FindByIdAsync(model.Id.ToString());

        if (user is null)
        {
            return IdNotFoundError;
        }

        var isOldPasswordCorrect = await this.userManager.CheckPasswordAsync(user, model.OldPassword);

        if (!isOldPasswordCorrect)
        {
            return IncorrectPasswordError;
        }

        user.PasswordHash = await this.userManager.HashPasswordAsync(user, model.NewPassword);
        this.data.SaveChanges();

        return true;
    }

    public async Task<OperationResult> Delete(Guid id)
    {
        var userToDelete = await this.userManager.FindByIdAsync(id.ToString());

        if (userToDelete is null)
        {
            return IdNotFoundError;
        }

        var result = await this.userManager.DeleteAsync(userToDelete);

        if (!result.Succeeded)
        {
            return IdNotFoundError;
        }

        return true;
    }
}
