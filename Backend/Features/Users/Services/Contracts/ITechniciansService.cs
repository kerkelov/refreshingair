﻿using RefreshingAir.Features.Users.Models.ServiceModels;
using RefreshingAir.Shared.ResultObject;
using Shared.ConventionalServices;

namespace RefreshingAir.Features.Users.Services.Contracts;

public interface ITechniciansService : ITransientService
{
    Task<OperationValueResult<Guid>> Create(CreateTechnicianModel model);

    Task<OperationValueResult<TechnicianDetailsModel>> GetById(Guid id);

    Task<OperationValueResult<IEnumerable<TechnicianListingModel>>> GetAll();

    Task<OperationResult> Update(UpdateTechnicianModel model);

    Task<OperationResult> ChangePassword(ChangePasswordModel model);

    Task<OperationResult> Delete(Guid id);
}