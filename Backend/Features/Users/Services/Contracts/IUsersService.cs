﻿using RefreshingAir.Features.Users.Models.ServiceModels;
using RefreshingAir.Shared.ResultObject;
using Shared.ConventionalServices;

namespace RefreshingAir.Features.Users.Services.Contracts;

public interface IUsersService : ITransientService
{
    Task<OperationValueResult<Guid>> Create(CreateUserModel model);

    Task<OperationValueResult<UserDetailsModel>> GetById(Guid id);

    Task<OperationValueResult<IEnumerable<UserListingModel>>> GetAll();

    Task<OperationResult> Update(UpdateUserModel model);

    Task<OperationResult> ChangePassword(ChangePasswordModel model);

    Task<OperationResult> Delete(Guid id);
}