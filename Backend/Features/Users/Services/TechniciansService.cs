﻿using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.Orders.Services.Contracts;
using RefreshingAir.Features.Users.Models.ServiceModels;
using RefreshingAir.Features.Users.Services.Contracts;
using RefreshingAir.Infrastructure.Extensions;
using RefreshingAir.Infrastructure.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared.ResultObject;
using System.Dynamic;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Users.Services;

public class TechniciansService : ITechniciansService
{
    private readonly AppDbContext data;
    private readonly IUserManager userManager;

    public TechniciansService(AppDbContext dbContext, IUserManager userManager)
    {
        data = dbContext;
        this.userManager = userManager;
    }

    public async Task<OperationValueResult<Guid>> Create(CreateTechnicianModel model)
    {
        var user = model.To<User>();
        var result = await this.userManager.CreateAsync(user, model.Password);
        await this.userManager.AddToRoleAsync(user, TechnicianRoleName);

        if (!result.Succeeded)
        {
            return new(result.ErrorMessage());
        }

        var createdUser = await this.userManager.FindByEmailAsync(model.Email);
        var technicianToCreate = new Technician() { Location = model.Location, User = createdUser };

        this.data.Add(technicianToCreate);

        await this.data.SaveChangesAsync();
        var createdTechnician = await this.data.Technicians.FirstOrDefaultAsync(technician => technician.UserId == createdUser.Id);

        return createdTechnician.Id;
    }

    public async Task<OperationValueResult<TechnicianDetailsModel>> GetById(Guid id)
    {
        var technician = await GetTechnicianByIdAsync(id);

        if (technician is null)
        {
            return new(IdNotFoundError);
        }

        var user = await this.userManager.FindByIdAsync(technician.UserId.ToString());
        var role = (await this.userManager.GetRolesAsync(user))[0];

        var technicianDetailsModel = user.To<TechnicianDetailsModel>();
        technicianDetailsModel.Id = id;
        technicianDetailsModel.Location = technician.Location;
        technicianDetailsModel.Role = role;

        return technicianDetailsModel;
    }

    public async Task<OperationResult> Update(UpdateTechnicianModel model)
    {
        var technician = await GetTechnicianByIdAsync(model.Id);

        if (technician is null)
        {
            return IdNotFoundError;
        }

        technician.User.MapAgainst(model);
        technician.User.Id = technician.UserId;
        technician.Location = model.Location;

        await this.data.SaveChangesAsync();

        return true;
    }

    public async Task<OperationResult> ChangePassword(ChangePasswordModel model)
    {
        var technician = await GetTechnicianByIdAsync(model.Id);

        if (technician is null)
        {
            return IdNotFoundError;
        }

        var isOldPasswordCorrect = await this.userManager.CheckPasswordAsync(technician.User, model.OldPassword);

        if (!isOldPasswordCorrect)
        {
            return "Password is incorrect";
        }

        technician.User.PasswordHash = await this.userManager.HashPasswordAsync(technician.User, model.NewPassword);
        this.data.SaveChanges();

        return true;
    }
    public async Task<OperationResult> Delete(Guid id)
    {
        var technician = await GetTechnicianByIdAsync(id);

        if (technician is null)
        {
            return IdNotFoundError;
        }

        //var technicianHaveActiveOrders = technician.Orders.Any();

        //if (technicianHaveActiveOrders)
        //{
        //    return "This technician cannot be deleted because he has active orders";
        //}

        var user = await this.userManager.FindByIdAsync(technician.UserId.ToString());
        user.IsDeleted = true;
        technician.IsDeleted = true;
        await this.data.SaveChangesAsync();

        return true;
    }

    private async Task<Technician> GetTechnicianByIdAsync(Guid id)
        => await this.data
           .Technicians
           .Include(technician => technician.Orders)
           .Include(technician => technician.User)
           .FirstOrDefaultAsync(technician => technician.Id == id);

    public async Task<OperationValueResult<IEnumerable<TechnicianListingModel>>> GetAll()
    {
        var technicians = await this.data.Technicians
            .Include(technician => technician.User)
            .Include(technician => technician.Orders)
            .ToListAsync();

        var result = new List<dynamic>();

        foreach (var technician in technicians)
        {
            dynamic flattenTechnician = new ExpandoObject();

            flattenTechnician.Id = technician.Id;
            flattenTechnician.Location = technician.Location;
            flattenTechnician.FirstName = technician.User.FirstName;
            flattenTechnician.LastName = technician.User.LastName;
            flattenTechnician.PhoneNumber = technician.User.PhoneNumber;
            flattenTechnician.Email = technician.User.Email;
            flattenTechnician.UserName = technician.User.UserName;
            flattenTechnician.ProfilePictureUrl = technician.User.ProfilePictureUrl;

            flattenTechnician.Role = (await this.userManager.GetRolesAsync(technician.User))[0];

            result.Add(flattenTechnician);
        };

        var asdf = result.Select(x => ((object)x).To<TechnicianListingModel>()).ToList();

        return new((IEnumerable<TechnicianListingModel>)asdf);
    }
}
