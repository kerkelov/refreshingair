using Microsoft.AspNetCore.Mvc;
using RefreshingAir.Features.Users.Models;
using RefreshingAir.Features.Users.Models.Requests;
using RefreshingAir.Features.Users.Models.Responses;
using RefreshingAir.Features.Users.Models.ServiceModels;
using RefreshingAir.Features.Users.Services;
using RefreshingAir.Features.Users.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Users.Controllers;

//TODO: should this controller be merged into the users controller
//and all users to be treated as one type of user in the fronted
//cuz now when getting user by id if its null you should check in the technicians
public class TechniciansController : ApiController
{
    private readonly ITechniciansService techniciansService;

    public TechniciansController(ITechniciansService techniciansService)
        => this.techniciansService = techniciansService;

    [HttpPost]
    public async Task<IActionResult> Create(CreateTechnicianRequest model)
    {
        var result = await this.techniciansService.Create(model.To<CreateTechnicianModel>());

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok(result.Value);
    }

    [HttpGet]
    [Route(RouteId)]
    public async Task<ActionResult<TechnicianDetailsResponse>> GetById(Guid id)
        => (await this.techniciansService.GetById(id))?
            .Value
            .To<TechnicianDetailsResponse>();


    [HttpGet]
    public async Task<IEnumerable<TechnicianListingResponse>> GetAll()
        => (await this.techniciansService.GetAll())
            .Value
            .Select(x => x.To<TechnicianListingResponse>())
            .ToList();


    [HttpPut]
    [Route(RouteId)]
    public async Task<IActionResult> Update(UpdateTechnicianRequest request)
    {
        var result = await this.techniciansService.Update(request.To<UpdateTechnicianModel>());

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }


    [HttpPost]
    [Route("password")]
    public async Task<IActionResult> ChangePassword(ChangePasswordRequest request)
    {
        var result = await this.techniciansService.ChangePassword(request.To<ChangePasswordModel>());

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }


    [HttpDelete]
    [Route(RouteId)]
    public async Task<IActionResult> Delete(Guid id)
    {
        var result = await this.techniciansService.Delete(id);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }
}