using Microsoft.AspNetCore.Mvc;
using RefreshingAir.Features.Users.Models.Requests;
using RefreshingAir.Features.Users.Models.Responses;
using RefreshingAir.Features.Users.Models.ServiceModels;
using RefreshingAir.Features.Users.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Users.Controllers;

public class UsersController : ApiController
{
    private readonly IUsersService usersService;

    public UsersController(IUsersService usersService)
        => this.usersService = usersService;

    [HttpPost]
    [Route("client")]
    public async Task<IActionResult> CreateClient(CreateUserRequest model)
    {
        var createUserModel = model.To<CreateUserModel>();
        createUserModel.Role = ClientRoleName;

        var result = await this.usersService.Create(createUserModel);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok(result.Value);
    }

    [HttpPost]
    [Route("admin")]
    public async Task<IActionResult> CreateAdmin(CreateUserRequest model)
    {
        var createUserModel = model.To<CreateUserModel>();
        createUserModel.Role = AdministratorRoleName;

        var result = await this.usersService.Create(createUserModel);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok(result.Value);
    }

    [HttpGet]
    [Route(RouteId)]
    public async Task<ActionResult<UserDetailsResponse>> GetById(Guid id)
        => (await this.usersService.GetById(id)).Value?.To<UserDetailsResponse>();

    [HttpGet]
    public async Task<ActionResult<IEnumerable<UserListingResponse>>> GetAll()
        => new((await this.usersService.GetAll())
           .Value
           .To<UserListingResponse>());

    [HttpPut(RouteId)]
    public async Task<IActionResult> Update(UpdateUserRequest request)
    {
        var result = await this.usersService.Update(request.To<UpdateUserModel>());

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }

    [HttpPost]
    [Route("password")]
    public async Task<IActionResult> ChangePassword(ChangePasswordRequest request)
    {
        var result = await this.usersService.ChangePassword(request.To<ChangePasswordModel>());

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }

    [HttpDelete(RouteId)]
    public async Task<IActionResult> Delete(Guid id)
    {
        var result = await this.usersService.Delete(id);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }
}