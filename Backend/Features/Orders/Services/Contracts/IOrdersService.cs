﻿using RefreshingAir.Features.Orders.Models.ServiceModels;
using RefreshingAir.Shared.ResultObject;
using Shared.ConventionalServices;

namespace RefreshingAir.Features.Orders.Services.Contracts;

public interface IOrdersService : ITransientService
{
    Task<OperationValueResult<Guid>> Create(CreateOrderModel model);

    Task<OperationValueResult<OrderDetailsModel>> GetById(Guid id);

    Task<OperationValueResult<IEnumerable<OrderListingModel>>> GetAll();

    Task<OperationResult> Update(UpdateOrderModel model);

    Task<OperationResult> Delete(Guid id);
}