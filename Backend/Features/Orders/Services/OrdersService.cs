﻿using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.Orders.Models.ServiceModels;
using RefreshingAir.Features.Orders.Services.Contracts;
using RefreshingAir.Infrastructure.Extensions;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared.ResultObject;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Orders.Services;

public class OrdersService : IOrdersService
{
    private readonly AppDbContext data;

    public OrdersService(AppDbContext dbContext)
        => this.data = dbContext;

    public async Task<OperationValueResult<Guid>> Create(CreateOrderModel model)
    {
        var order = model.To<Order>();

        this.data.Add(order);
        await this.data.SaveChangesAsync();

        return order.Id;
    }

    public async Task<OperationValueResult<OrderDetailsModel>> GetById(Guid id)
    {
        var order = await this.data.Orders.FindByIdAsync(id);

        if (order is null)
        {
            return new(IdNotFoundError);
        }

        return order.To<OrderDetailsModel>();
    }

    public async Task<OperationValueResult<IEnumerable<OrderListingModel>>> GetAll()
        => new((await this.data
                .Orders
                .ToListAsync())
                .To<OrderListingModel>());
    public async Task<OperationResult> Update(UpdateOrderModel model)
    {
        var order = await this.data.Orders.FindByIdAsync(model.Id);

        if (order is null)
        {
            return IdNotFoundError;
        }

        order.MapAgainst(model);
        await this.data.SaveChangesAsync();

        return true;
    }

    public async Task<OperationResult> Delete(Guid id)
    {
        var order = await this.data
            .Orders
            .Include(order => order.PartsNeeded)
            .FindByIdAsync(id);

        if (order is null)
        {
            return IdNotFoundError;
        }

        //TODO: is this the best way to do cascade soft deletion
        //or is there some better approach to be implemented inside of the dbContext
        //when applying the auditInfo

        this.data.RemoveRange(order.PartsNeeded);
        this.data.Remove(order);

        await this.data.SaveChangesAsync();

        return true;
    }
}
