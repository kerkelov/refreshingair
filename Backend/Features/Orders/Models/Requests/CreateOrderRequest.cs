﻿using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Orders.Models.ServiceModels;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Features.Orders.Models.Requests;

//TODO: where is better to put IMapTo here or put IMapFrom in the CreateOrderModel
public class CreateOrderRequest : IMapTo<CreateOrderModel>
{
    [Required]
    public Guid AirConditionerId { get; set; }

    [Required]
    public Guid ClientId { get; set; }

    [Required]
    public Guid TechnicianId { get; set; }

    [Required]
    public DateTime Deadline { get; set; }
}