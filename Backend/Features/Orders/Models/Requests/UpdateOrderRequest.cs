﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.Orders.Models.ServiceModels;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Features.Orders.Models.Requests;

public class UpdateOrderRequest : IMapTo<UpdateOrderModel>
{

    [Required]
    public Guid Id { get; set; }

    [Required]
    public Guid AirConditionerId { get; set; }

    [Required]
    public Guid ClientId { get; set; }

    [Required]
    public Guid TechnicianId { get; set; }

    [Required]
    public DateTime Deadline { get; set; }

    [Required]
    public Status Status { get; set; }

    [Required]
    public DateTime? Started { get; set; }

    [Required]
    public DateTime? Finished { get; set; }

}