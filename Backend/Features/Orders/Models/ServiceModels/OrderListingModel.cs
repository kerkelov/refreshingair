﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;

namespace RefreshingAir.Features.Orders.Models.ServiceModels;

public class OrderListingModel : IMapFrom<Order>
{
    public Guid Id { get; set; }

    public Guid AirConditionerId { get; set; }

    public Guid ClientId { get; set; }

    public Guid TechnicianId { get; set; }

    public DateTime Deadline { get; set; }

    public Status Status { get; set; }

    public DateTime? Started { get; set; }

    public DateTime? Finished { get; set; }
}