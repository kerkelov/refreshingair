using Microsoft.AspNetCore.Mvc;
using RefreshingAir.Features.Orders.Models.Requests;
using RefreshingAir.Features.Orders.Models.Responses;
using RefreshingAir.Features.Orders.Models.ServiceModels;
using RefreshingAir.Features.Orders.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Orders.Controllers;

public class OrdersController : ApiController
{
    private readonly IOrdersService ordersService;

    public OrdersController(IOrdersService ordersService)
        => this.ordersService = ordersService;

    [HttpPost]
    public async Task<IActionResult> Create(CreateOrderRequest request)
    {
        var model = request.To<CreateOrderModel>();
        var result = await this.ordersService.Create(model);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok(result.Value);
    }

    [HttpGet]
    [Route(RouteId)]
    public async Task<ActionResult<OrderDetailsResponse>> GetById(Guid id)
    {
        var result = await this.ordersService.GetById(id);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        var response = result.Value.To<OrderDetailsResponse>();

        return Ok(response);
    }

    [HttpGet]
    public async Task<IEnumerable<OrderListingResponse>> GetAll()
        => (await this.ordersService.GetAll())
           .Value
           .To<OrderListingResponse>();

    [HttpPut]
    public async Task<IActionResult> Update(UpdateOrderRequest request)
    {
        var model = request.To<UpdateOrderModel>();
        var result = await this.ordersService.Update(model);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }

    [HttpDelete]
    public async Task<IActionResult> Delete(Guid id)
    {
        var result = await this.ordersService.Delete(id);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }
}