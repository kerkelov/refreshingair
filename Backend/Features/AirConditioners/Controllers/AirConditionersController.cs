using Microsoft.AspNetCore.Mvc;
using RefreshingAir.Features.AirConditioners.Models.Requests;
using RefreshingAir.Features.AirConditioners.Models.Responses;
using RefreshingAir.Features.AirConditioners.Models.ServiceModels;
using RefreshingAir.Features.AirConditioners.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.AirConditioners.Controllers;

public class AirConditionersController : ApiController
{
    private readonly IAirConditionersService airConditionersService;

    public AirConditionersController(IAirConditionersService airConditionersService)
        => this.airConditionersService = airConditionersService;

    [HttpPost]
    public async Task<IActionResult> Create(CreateAirConditionerRequest request)
    {
        var model = request.To<CreateAirConditionerModel>();
        var result = await this.airConditionersService.Create(model);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok(result.Value);
    }

    [HttpGet]
    [Route(RouteId)]
    public async Task<ActionResult<AirConditionerDetailsResponse>> GetById(Guid id)
    {
        var result = await this.airConditionersService.GetById(id);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        var response = result.Value.To<AirConditionerDetailsResponse>();

        return response;
    }

    [HttpGet]
    public async Task<IEnumerable<AirConditionerListingResponse>> GetAll()
        => (await this.airConditionersService.GetAll())
           .Value
           .To<AirConditionerListingResponse>();


    [HttpPut(RouteId)]
    public async Task<IActionResult> Update(UpdateAirConditionerRequest request)
    {
        var model = request.To<UpdateAirConditionerModel>();
        var result = await this.airConditionersService.Update(model);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }

    [HttpDelete(RouteId)]
    public async Task<IActionResult> Delete(Guid id)
    {
        var result = await this.airConditionersService.Delete(id);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }
}