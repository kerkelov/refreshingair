﻿using RefreshingAir.Features.AirConditioners.Models.ServiceModels;
using RefreshingAir.Shared.ResultObject;
using Shared.ConventionalServices;

namespace RefreshingAir.Features.AirConditioners.Services.Contracts;

public interface IAirConditionersService : ITransientService
{
    Task<OperationValueResult<Guid>> Create(CreateAirConditionerModel model);

    Task<OperationValueResult<AirConditionerDetailsModel>> GetById(Guid id);

    Task<OperationValueResult<IEnumerable<AirConditionerListingModel>>> GetAll();

    Task<OperationResult> Update(UpdateAirConditionerModel model);

    Task<OperationResult> Delete(Guid id);
}