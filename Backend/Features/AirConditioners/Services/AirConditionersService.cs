﻿using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.AirConditioners.Models.ServiceModels;
using RefreshingAir.Features.AirConditioners.Services.Contracts;
using RefreshingAir.Infrastructure.Extensions;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared.ResultObject;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.AirConditioners.Services;

public class AirConditionersService : IAirConditionersService
{
    private readonly AppDbContext data;

    public AirConditionersService(AppDbContext dbContext)
        => data = dbContext;

    public async Task<OperationValueResult<Guid>> Create(CreateAirConditionerModel model)
    {
        var airConditioner = model.To<AirConditioner>();

        this.data.Add(airConditioner);
        await this.data.SaveChangesAsync();

        return airConditioner.Id;
    }

    public async Task<OperationValueResult<AirConditionerDetailsModel>> GetById(Guid id)
    {
        var airConditioner = await this.data.AirConditioners.FindByIdAsync(id);

        if (airConditioner is null)
        {
            return new(IdNotFoundError);
        }

        return airConditioner.To<AirConditionerDetailsModel>();
    }

    public async Task<OperationValueResult<IEnumerable<AirConditionerListingModel>>> GetAll()
    {
        var asdf = await this.data
            .AirConditioners
            .ToListAsync();

        return new(asdf.To<AirConditionerListingModel>());
    }


    public async Task<OperationResult> Update(UpdateAirConditionerModel model)
    {
        var airConditioner = await this.data.AirConditioners.FindByIdAsync(model.Id);

        if (airConditioner is null)
        {
            return IdNotFoundError;
        }

        airConditioner.MapAgainst(model);
        await this.data.SaveChangesAsync();

        return true;
    }

    public async Task<OperationResult> Delete(Guid id)
    {
        var airConditioner = await this.data.AirConditioners.FindByIdAsync(id);

        if (airConditioner is null)
        {
            return false;
        }

        this.data.Remove(airConditioner);
        await this.data.SaveChangesAsync();

        return true;
    }
}
