﻿using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.AirConditioners.Models.ServiceModels;

namespace RefreshingAir.Features.AirConditioners.Models.Responses;

public class AirConditionerDetailsResponse : IMapFrom<AirConditionerDetailsModel>
{
    public Guid Id { get; set; }

    public string Model { get; set; }

    public string Country { get; set; }

    public string Manufacturer { get; set; }

    public string PictureUrl { get; set; }

    public double Price { get; set; }

    public string Description { get; set; }
    public AirConditionerType Type { get; set; }
}