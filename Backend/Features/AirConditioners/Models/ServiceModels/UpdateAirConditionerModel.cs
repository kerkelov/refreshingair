﻿using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;

namespace RefreshingAir.Features.AirConditioners.Models.ServiceModels;

public class UpdateAirConditionerModel : IMapTo<AirConditioner>
{
    public Guid Id { get; set; }

    public string Model { get; set; }

    public string Country { get; set; }

    public string Manufacturer { get; set; }

    public string PictureUrl { get; set; }

    public double Price { get; set; }

    public string Description { get; set; }

    public AirConditionerType Type { get; set; }
}