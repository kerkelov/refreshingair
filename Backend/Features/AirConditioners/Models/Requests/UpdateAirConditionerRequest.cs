﻿using System.ComponentModel.DataAnnotations;
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.AirConditioners.Models.ServiceModels;

namespace RefreshingAir.Features.AirConditioners.Models.Requests;

public class UpdateAirConditionerRequest : IMapTo<UpdateAirConditionerModel>
{
    [Required]
    public Guid Id { get; set; }

    [Required]
    public string Model { get; set; }

    [Required]
    public string Country { get; set; }

    [Required]
    public string Manufacturer { get; set; }

    [Required]
    public string PictureUrl { get; set; }

    [Required]
    public double Price { get; set; }

    [Required]
    public string Description { get; set; }

    [Required]
    public AirConditionerType Type { get; set; }
}