using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.Authentication.Models.Requests;
using RefreshingAir.Features.Authentication.Models.Responses;
using RefreshingAir.Features.Authentication.Models.ServiceModels;
using RefreshingAir.Features.Authentication.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared;

namespace RefreshingAir.Features.Authentication.Controllers;

[AllowAnonymous]
public class AuthenticationController : ApiController
{
    private readonly IAuthenticationService identityService;

    public AuthenticationController(IAuthenticationService identityService)
        => this.identityService = identityService;

    [HttpPost]
    [Route("login")]
    public async Task<ActionResult<LoginResponse>> Login(LoginRequest request)
    {
        var model = request.To<LoginModel>();
        var result = await this.identityService.Login(model);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return result.Value.To<LoginResponse>();
    }
}