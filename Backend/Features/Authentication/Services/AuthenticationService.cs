using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RefreshingAir.Features.Authentication.Models.ServiceModels;
using RefreshingAir.Features.Authentication.Services.Contracts;
using RefreshingAir.Infrastructure.Services.Contracts;
using RefreshingAir.Shared.ResultObject;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Authentication.Services;

public class AuthenticationService : IAuthenticationService
{
    private readonly AppSettings appSettings;
    private readonly IUserManager userManager;

    public AuthenticationService(IUserManager userManager, IOptions<AppSettings> appSettingsOptions)
    {
        this.appSettings = appSettingsOptions.Value;
        this.userManager = userManager;
    }

    public async Task<OperationValueResult<TokenModel>> Login(LoginModel model)
    {
        var user = await this.userManager.FindByEmailAsync(model.Email);

        if (user is null)
        {
            return new(EmailNotFoundError);
        }

        var isPasswordValid = await this.userManager.CheckPasswordAsync(user, model.Password);

        if (!isPasswordValid)
        {
            return new(IncorrectPasswordError);
        }

        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(appSettings.Secret ?? SecondaryAppSecret);

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[] {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(
                new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        return new TokenModel(jwt);
    }
}