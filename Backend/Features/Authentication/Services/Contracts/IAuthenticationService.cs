using RefreshingAir.Features.Authentication.Models.ServiceModels;
using RefreshingAir.Shared.ResultObject;
using Shared.ConventionalServices;

namespace RefreshingAir.Features.Authentication.Services.Contracts;

public interface IAuthenticationService : ITransientService
{
    Task<OperationValueResult<TokenModel>> Login(LoginModel model);
}