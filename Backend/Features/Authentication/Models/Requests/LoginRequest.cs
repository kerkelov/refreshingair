using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Authentication.Models.ServiceModels;
using System.ComponentModel.DataAnnotations;

namespace RefreshingAir.Features.Authentication.Models.Requests;

public class LoginRequest : IMapTo<LoginModel>
{
    [Required]
    [EmailAddress]
    public string Email { get; set; }

    [Required]
    public string Password { get; set; }
}