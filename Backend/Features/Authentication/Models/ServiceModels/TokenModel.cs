﻿namespace RefreshingAir.Features.Authentication.Models.ServiceModels;

public class TokenModel
{
    public TokenModel(string jwt)
    {
        this.JwtToken = jwt;
    }

    public string JwtToken { get; set; }
}