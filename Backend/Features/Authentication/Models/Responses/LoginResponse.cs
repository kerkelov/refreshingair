
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Features.Authentication.Models.ServiceModels;

namespace RefreshingAir.Features.Authentication.Models.Responses;

public class LoginResponse : IMapFrom<TokenModel>
{
    public string JwtToken { get; set; }
}
