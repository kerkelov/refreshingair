﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;

namespace RefreshingAir.Features.Orders.Models.ServiceModels;

public class CreateAirConOrderModel : IMapTo<AirConOrder>
{
    public Guid AirConditionerId { get; set; }

    public Guid ClientId { get; set; }
}