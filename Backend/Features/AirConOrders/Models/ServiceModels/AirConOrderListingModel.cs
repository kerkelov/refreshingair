﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;

namespace RefreshingAir.Features.Orders.Models.ServiceModels;

public class AirConOrderListingModel : IMapFrom<AirConOrder>
{
    public Guid Id { get; set; }

    public Guid AirConditionerId { get; set; }

    public Guid ClientId { get; set; }

    public DateTime CreatedOn { get; set; }
}