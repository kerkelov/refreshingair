﻿
using Infrastructure.Mapping.Contracts;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.Orders.Models.ServiceModels;

namespace RefreshingAir.Features.Orders.Models.Responses;

public class AirConOrderListingResponse : IMapFrom<AirConOrderListingModel>
{
    public Guid Id { get; set; }

    public Guid AirConditionerId { get; set; }

    public Guid ClientId { get; set; }

    public string CreatedOn { get; set; }
}