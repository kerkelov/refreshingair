using Microsoft.AspNetCore.Mvc;
using RefreshingAir.Features.Orders.Models.Requests;
using RefreshingAir.Features.Orders.Models.Responses;
using RefreshingAir.Features.Orders.Models.ServiceModels;
using RefreshingAir.Features.Orders.Services.Contracts;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Orders.Controllers;

public class AirConOrdersController : ApiController
{
    private readonly IAirConOrdersService airConOrdersService;

    public AirConOrdersController(IAirConOrdersService ordersService)
        => this.airConOrdersService = ordersService;

    [HttpPost]
    public async Task<IActionResult> Create(CreateAirConOrderRequest request)
    {
        var model = request.To<CreateAirConOrderModel>();
        var result = await this.airConOrdersService.Create(model);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok(result.Value);
    }

    [HttpGet]
    public async Task<IEnumerable<AirConOrderListingResponse>> GetAll()
        => (await this.airConOrdersService.GetAll())
           .Value
           .To<AirConOrderListingResponse>();

    [HttpDelete(RouteId)]
    public async Task<IActionResult> Delete(Guid id)
    {
        var result = await this.airConOrdersService.Delete(id);

        if (result.Failure)
        {
            return BadRequest(result.ErrorMessage);
        }

        return Ok();
    }
}