﻿using RefreshingAir.Features.Orders.Models.ServiceModels;
using RefreshingAir.Shared.ResultObject;
using Shared.ConventionalServices;

namespace RefreshingAir.Features.Orders.Services.Contracts;

public interface IAirConOrdersService : ITransientService
{
    Task<OperationValueResult<Guid>> Create(CreateAirConOrderModel model);

    Task<OperationValueResult<IEnumerable<AirConOrderListingModel>>> GetAll();

    Task<OperationResult> Delete(Guid id);
}