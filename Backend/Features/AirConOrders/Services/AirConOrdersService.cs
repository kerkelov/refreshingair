﻿using Microsoft.EntityFrameworkCore;
using RefreshingAir.Data;
using RefreshingAir.Data.Entities;
using RefreshingAir.Features.Orders.Models.ServiceModels;
using RefreshingAir.Features.Orders.Services.Contracts;
using RefreshingAir.Infrastructure.Extensions;
using RefreshingAir.Mapping.Extensions;
using RefreshingAir.Shared.ResultObject;
using static RefreshingAir.Shared.Constants.WebConstants;

namespace RefreshingAir.Features.Orders.Services;

public class AirConOrdersService : IAirConOrdersService
{
    private readonly AppDbContext data;

    public AirConOrdersService(AppDbContext dbContext)
        => this.data = dbContext;

    public async Task<OperationValueResult<Guid>> Create(CreateAirConOrderModel model)
    {
        var airConOrder = model.To<AirConOrder>();

        this.data.Add(airConOrder);
        await this.data.SaveChangesAsync();

        return airConOrder.Id;
    }

    public async Task<OperationValueResult<IEnumerable<AirConOrderListingModel>>> GetAll()
        => new((await this.data
                .AirConOrders
                .ToListAsync())
                .To<AirConOrderListingModel>());
    public async Task<OperationResult> Delete(Guid id)
    {
        var airConOrder = await this.data
            .AirConOrders
            .FindByIdAsync(id);

        if (airConOrder is null)
        {
            return IdNotFoundError;
        }

        this.data.Remove(airConOrder);
        await this.data.SaveChangesAsync();

        return true;
    }
}
