using RefreshingAir.Infrastructure.Extensions;
using static RefreshingAir.Shared.Constants.WebConstants;

var builder = WebApplication.CreateBuilder(args);

builder
    .Services
    .AddEndpointsApiExplorer()
    .SetupCors()
    .SetupSwaggerGen()
    .SetupControllers()
    .SetupDbContext(builder.Configuration)
    .SetupIdentity()
    .SetupJwtAuthentication(builder.Services.GetAppSettings(builder.Configuration))
    .SetupConventionalServices();

var app = builder.Build();

app
    .UseCors(AllowLocalHostAngularCorsPolicy)
    .UseDeveloperExceptionPage()
    .SetupSwaggerUI(app.Environment)
    .SetupAutomapper()
    .SetupSeeding(app.Services)
    .UseHttpsRedirection()
    .UseAuthorization();

app.MapControllers();
app.Run();
